#include <jni.h>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

bool in_array(const std::string &value, const std::vector<string> &array) {
    return std::find(array.begin(), array.end(), value) != array.end();
}

vector<string> javaStringArrToVector(JNIEnv *env, jobjectArray jstringArr) {
    vector<string> stringVec;

    // Get length
    int len = env->GetArrayLength(jstringArr);

    for(int i = 0; i < len; i++) {
        // Cast array element to string
        jstring jstr =(jstring)(env->GetObjectArrayElement(jstringArr, i));

        // Convert Java string to std::string
        const jsize strLen = env->GetStringUTFLength(jstr);
        const char *charBuffer = env->GetStringUTFChars(jstr,(jboolean *) 0);
        string str(charBuffer, strLen);

        // Push back string to vector
        stringVec.push_back(str);

        // Release memory
        env->ReleaseStringUTFChars(jstr, charBuffer);
        env->DeleteLocalRef(jstr);
    }

    return stringVec;
}

jstring getIfContains(JNIEnv *env, jobjectArray jstringArr, string requiredNode) {
    bool isContains = in_array(requiredNode, javaStringArrToVector(env, jstringArr));
    if(isContains) {
        return env->NewStringUTF(requiredNode.c_str());;
    } else {
        return NULL;
    }
}

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdOfferFragment(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/offer_fragment")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdLoadingLayout(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/fl_al_login_loading")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdAuthField(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/et_al_driver_id")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdAuthLoadingMsg(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/tv_al_loading_main_message")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdSwitcher(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/free_busy_switch")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdRideButton(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/fl_frc_ride_btn")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdMsgDialog(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/tv_dtb_message")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdBookingLayout(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/ll_afb_buttons_layout")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdBookingOrderInfoLayout(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/future_order_details_map")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdDriverToolbar(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/driver_app_toolbar")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdToolbar(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/toolbar")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdHistoryOrderTitle(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/tv_history_breakdown_trip_details_title")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdAssignedOrder(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/radar_parent")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdInfoDialog(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/tv_di_dialog_message")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdQuestionDialog(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/ddPositiveBtn")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_gett0resIdStats(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.gettaxi.dbx.android:id/rv_stats")); }

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdLoginField(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/editLogin")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdStartViewBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/buttonLogin")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdSwitcher(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/btnBusy")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdBtnYes(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("android:id/button1")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdBtnNo(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("android:id/button2")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdOffersCancelBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/buttonFail")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdLblBusy(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/lblBusy")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdDriverAvatar(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/llEditPhoto")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdMessages(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/listMessages")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdWorking(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/ivWorking")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_opteum0resIdStatusBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.opteum.opteumTaxi:id/buttonStatus")); }

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_uber0resIdLauncherLayout(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.ubercab.driver:id/ub__launch_root")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_uber0resIdBtnOrderRide(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.ubercab.driver:id/ub__online_viewgroup_do_panel_slide_to_confirm")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_uber0resIdSwitcher(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.ubercab.driver:id/ub__app_state_switch")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_uber0resIdOfferLayout(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.ubercab.driver:id/ub__dispatch_primary_touch_area")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_uber0resIdEarnings(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.ubercab.driver:id/ub__alloy_earnings_recycler_view")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_uber0resIdAlert(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.ubercab.driver:id/alertTitle")); }

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdRideButtonOk(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/taximeter_ok")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdRatingFeedback(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/rating_feedback")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdGridButtonText(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/dashboard_item_name_text_view")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdBtnHome(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/btn_home")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdBtnYes(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("android:id/button1")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdProgressIndicator(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("android:id/progress")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdProgressMessage(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("android:id/message")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdAuthContent(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/auth_content")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdAuthPhone(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/et_phone_number")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdAuthParkBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/park_ready")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdMessage(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/message")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdSwitcher(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/online_switch")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdRequest(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/activity_request_fragment")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdRequestView(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/activity_request_view_root")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdChooseLanguage(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/view_choose_language")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_yandex0resIdDriverBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.yandex.taximeter:id/driver_button")); }

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdOrderContainer(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/order_info_container")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdActionBar(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/actionbar_actions")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdActionTitle(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/actionbar_title")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdCheckbox(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("android:id/checkbox")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdCheckboxDbg(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/checkbox")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdAlertTitle(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/alertTitle")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdLogin(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/loginbutton")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdLoading(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/login_root")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdHomeBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/actionbar_home_btn")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdActionBtn(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/info_action_button")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_taxsee0resIdChannels(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.taxsee.driver:id/channels")); }

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_lyft0resIdBtnToggle(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.lyft.android.driver:id/driver_mode_toggle")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_lyft0resIdDriverRideContainer(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.lyft.android.driver:id/driver_screen_container")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_lyft0valueBtnToggleOnline(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("online")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_lyft0valueBtnToggleLastRide(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("last ride")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_lyft0resIdAcceptOrder(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("com.lyft.android.driver:id/accept_tap_text_view")); }

extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdBtnSetBusy(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/layoutToolbarRobotOff")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdBtnPositive(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/buttonPositive")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdBtnOfferAccept(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/textViewOrderOfferAccept")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdBtnArrived(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/buttonOrderArrived")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdBtnGo(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/buttonOrderOnWay")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdBtnDone(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/buttonTaximeterOrderFinish")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdDialogMessage(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/textViewDialogMessage")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdSplash(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/imageViewSplashLogo")); }
extern "C" JNIEXPORT jstring JNICALL Java_com_opteum_handy_jni_JniHandy_citymobil0resIdRegister(JNIEnv *env, jobject, jobjectArray jstrArr) { return getIfContains(env, jstrArr, string("ru.citymobil.driver:id/buttonRegisterPhone")); }


