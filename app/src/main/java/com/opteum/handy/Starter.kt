package com.opteum.handy

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.mvp.Interactor_Fcm
import com.opteum.handy.mvp.Interactor_Init
import com.opteum.handy.network.responses.Response_Init
import com.opteum.handy.services.HandyAccessibilityService
import com.opteum.handy.ui.ActivityContainer_Main
import com.opteum.handy.ui.Activity_PaymentInfo
import com.opteum.handy.ui.base.ifFalse
import com.opteum.handy.ui.base.ifTrue
import com.opteum.handy.utils.AccessibilityUtil

class Starter : AppCompatActivity() {
    private val ctx: Context = App.kodein.instance()
    private val interactorInit = Interactor_Init()
    private val interactorFcm = Interactor_Fcm()

    private var isAccessibilityOpened = false
    private var isMinVersionMarketOpened = false

    private var initResponse: Response_Init? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starter)
    }

    override fun onResume() {
        super.onResume()
        stage0_init { stage1_checkVersion { stage2_checkAccessibility { stage3_checkPaymentTime { stage4_sendPushToken { stage_last_complete() } } } } }
    }

    private fun stage0_init(onSuccess: (() -> Unit)) {
        interactorInit.init { result ->
            initResponse = result.data
            onSuccess.invoke()
        }
    }

    // todo make better logic
    private fun stage1_checkVersion(onSuccess: (() -> Unit)) {
        if (isMinVersionMarketOpened) {
            isMinVersionMarketOpened = false
            finish()
        } else {
            initResponse?.let {
                if (it.minVersionCode > BuildConfig.VERSION_CODE) {
                    isMinVersionMarketOpened = true
                    Toast.makeText(ctx, getString(R.string.text_your_app_outdated_update_it), Toast.LENGTH_LONG).show()
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)))
                    } catch (e: android.content.ActivityNotFoundException) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)))
                    }
                }
            }

            isMinVersionMarketOpened.ifFalse(onSuccess)
        }
    }

    private fun stage2_checkAccessibility(onSuccess: (() -> Unit)) {
        val isEnabled = AccessibilityUtil.isAccessibilityEnabled(this, HandyAccessibilityService::class.java)
        if (!isEnabled) {
            Toast.makeText(applicationContext, "${getString(R.string.text_enable_service_to_continue)} - \"${getString(R.string.app_name)}\"", Toast.LENGTH_LONG).show()

            if (!isAccessibilityOpened) {
                isAccessibilityOpened = true
                AccessibilityUtil.startAccessibilitySettings(this)
            } else {
                isAccessibilityOpened = false
                finish()
            }
        }

        isEnabled.ifTrue(onSuccess)
    }

    private fun stage3_checkPaymentTime(onSuccess: (() -> Unit)) {
        val isPaymentTime = initResponse?.isPaymentTime ?: false

        if (isPaymentTime) {
            startActivity(Intent(this, Activity_PaymentInfo::class.java))
            finish()
        } else {
            onSuccess.invoke()
        }
    }

    private fun stage4_sendPushToken(onSuccess: (() -> Unit)) {
        interactorFcm.sendToken({ onSuccess.invoke() })
    }

    private fun stage_last_complete() {
        startActivity(Intent(this, ActivityContainer_Main::class.java))
        finish()
    }
}