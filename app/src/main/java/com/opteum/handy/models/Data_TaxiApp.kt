package com.opteum.handy.models

class Data_TaxiApp(val id: String, val name: String, val iconResEnabled: Int, val iconResDisabled: Int, var isEnabled: Boolean = false) {
    companion object {
        const val ID_GETT = "GETT"
        const val ID_UBER = "UBER"
        const val ID_LYFT = "LYFT"
        const val ID_CITYMOBIL = "CITYMOBIL"
        const val ID_YANDEX = "YANDEX"
        const val ID_OPTEUM = "OPTEUM"
        const val ID_TAXSEE = "TAXSEE"
        const val NAME_GETT = "Gett"
        const val NAME_UBER = "Uber"
        const val NAME_LYFT = "Lyft (Beta)"
        const val NAME_CITYMOBIL = "Citymobil (Beta)"
        const val NAME_YANDEX = "Yandex"
        const val NAME_OPTEUM = "Opteum"
        const val NAME_TAXSEE = "TaxSee"
        const val PACKAGE_GETT = "com.gettaxi.dbx.android"
        const val PACKAGE_UBER = "com.ubercab.driver"
        const val PACKAGE_LYFT = "com.lyft.android.driver"
        const val PACKAGE_CITYMOBIL = "ru.citymobil.driver"
        const val PACKAGE_YANDEX = "ru.yandex.taximeter"
        const val PACKAGE_OPTEUM = "com.opteum.opteumTaxi"
        const val PACKAGE_TAXSEE = "com.taxsee.driver"
    }
}