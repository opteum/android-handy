package com.opteum.handy.models.cache

import com.opteum.handy.models.Data_TaxiApp

class TaxiAppsCache {
    private var cacheList: ArrayList<Data_TaxiApp>? = null

    fun get(): ArrayList<Data_TaxiApp>? {
        return cacheList
    }

    fun set(cacheList: ArrayList<Data_TaxiApp>) {
        this.cacheList = cacheList
    }
}