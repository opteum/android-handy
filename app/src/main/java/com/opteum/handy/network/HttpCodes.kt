package com.opteum.handy.network

object HttpCodes {
    val NOT_FOUND = 404
    val NOT_AUTH = 401
    val SUCCESS = 200
}