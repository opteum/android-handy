package com.opteum.handy.network.responses

import com.google.gson.annotations.SerializedName

class Response_Auth(@SerializedName("auth_token") val authToken: String)