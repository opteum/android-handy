package com.opteum.handy.network.responses

import com.google.gson.annotations.SerializedName

class Response_PaymentData(val tariffValue: Int, @SerializedName("expiredDate") val expiredDateMs: Long, val title: String = "", val description: String = "")