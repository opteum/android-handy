package com.opteum.handy.network.responses

import com.google.gson.annotations.SerializedName

class Response_Init(@SerializedName("minVersion") val minVersionCode: Int, val isPaymentTime: Boolean)
