package com.opteum.handy.network

import com.opteum.handy.network.responses.*
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Api {
    @FormUrlEncoded
    @POST("http://demo.opteum.ru/api/1/log")
    fun feedback(@Field("browser") dataJsonObject: String, @Field("secret") secretKey: String, @Field("stack") info: String = "HANDY FEEDBACK"): Call<AppResponse<Unit>>

    @FormUrlEncoded
    @POST("http://log.opteum.ru")
    fun log(@Field("pack") pack: String): Call<AppResponse<Unit>>

    @FormUrlEncoded
    @POST("init")
    fun init(@Field("appVersionCode") appVersionCode: String, @Field("locale") locale: String): Call<AppResponse<Response_Init>>

    @FormUrlEncoded
    @POST("fcm-token/save")
    fun sendToken(@Field("fcm_token") token: String): Call<AppResponse<Unit>>

    @FormUrlEncoded
    @POST("report-order-event")
    fun sendReportOrderEvent(@Field("taxiApp") taxiAppId: String): Call<AppResponse<Unit>>

    @FormUrlEncoded
    @POST("payment/rejection-reason")
    fun sendRejectReason(@Field("reason") reason: String): Call<AppResponse<Unit>>

    @POST("payment/info")
    fun getPaymentData(): Call<AppResponse<Response_PaymentData>>

    @POST("profile")
    fun getProfileData(): Call<AppResponse<Response_Profile>>

    @FormUrlEncoded
    @POST("profile/save")
    fun sendProfileData(@Field("phone") phone: String, @Field("name") name: String): Call<AppResponse<Unit>>

    @FormUrlEncoded
    @POST("register")
    fun register(@Field("email") email: String, @Field("password") password: String): Call<AppResponse<Unit>>

    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email") email: String, @Field("password") password: String): Call<AppResponse<Response_Auth>>

    @POST("logout")
    fun logout(): Call<AppResponse<Unit>>
}