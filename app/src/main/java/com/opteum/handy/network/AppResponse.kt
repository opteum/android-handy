package com.opteum.handy.network

// google json style
data class AppResponse<out T>(val data: T?, val error: AppError?, val success: Boolean, val status: Int) {
    data class AppError(val code: String, val message: String)
}