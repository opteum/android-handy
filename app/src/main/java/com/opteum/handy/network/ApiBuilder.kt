package com.opteum.handy.network

import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.BuildConfig
import com.opteum.handy.R
import com.opteum.handy.data.AppStrings
import com.opteum.handy.utils.AppUtil
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiBuilder {
    private val strings: AppStrings = App.kodein.instance()
    private val appUtil: AppUtil = App.kodein.instance()
    private val authToken: AuthToken = App.kodein.instance()

    fun build(): Api {
        val builder = Retrofit.Builder()
                .baseUrl(strings[R.string.url_base])
                .addConverterFactory(GsonConverterFactory.create())

        val httpClient = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }

        httpClient.addInterceptor { chain ->
            val b = chain.request().newBuilder()
            b.addHeader("device-id", appUtil.getDeviceId())
            b.addHeader("auth-token", authToken.getCurrentToken())

            return@addInterceptor chain.proceed(b.build())
        }


        val client = httpClient.build()
        val retrofit = builder.client(client).build()
        return retrofit.create(Api::class.java)
    }
}