package com.opteum.handy.network

import com.github.salomonbrys.kodein.instance
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppStrings
import com.opteum.handy.ui.base.InteractorResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

class AppApiCallExecutor {
    private val strings: AppStrings = App.kodein.instance()
    private val gson: Gson = App.kodein.instance()

    fun <T> executeApiCall(apiCall: Call<AppResponse<T>>, callback: (result: InteractorResult<T>) -> Unit,
                           additionalOnSuccess: ((response: AppResponse<T>?) -> Unit)? = null,
                           additionalOnFail: ((response: Response<AppResponse<T>>?, result: InteractorResult<T>) -> Unit)? = null) {
        val result = InteractorResult<T>()

        apiCall.enqueue(object : Callback<AppResponse<T>> {
            override fun onResponse(call: Call<AppResponse<T>>?, response: Response<AppResponse<T>>?) {
                response?.let { notNullResponse ->
                    result.data = notNullResponse.body()?.data
                    result.isSuccess = notNullResponse.code() == HttpCodes.SUCCESS

                    if (result.isSuccess) {
                        additionalOnSuccess?.invoke(response.body())
                    } else {
                        result.errorMessage = strings[R.string.text_response_error]

                        notNullResponse.errorBody()?.let { notNullErrorBody ->
                            var responseOfError: AppResponse<T>? = null

                            try {
                                responseOfError = gson.fromJson<AppResponse<T>>(notNullErrorBody.string(), AppResponse::class.java)
                            } catch (e: JsonSyntaxException) {
                            }

                            responseOfError?.let { it.error?.let { result.errorMessage = it.message } }
                        }

                        when {
                            response.code() == HttpCodes.NOT_AUTH -> result.errorMessage = strings[R.string.text_auth_error]
                            else -> {
                            }
                        }

                        additionalOnFail?.invoke(response, result)
                    }
                }

                callback.invoke(result)
            }

            override fun onFailure(call: Call<AppResponse<T>>?, t: Throwable?) {
                result.isSuccess = false

                result.errorMessage = strings[R.string.text_response_error]
                if (t is UnknownHostException) {
                    result.errorMessage = strings[R.string.text_cant_connect_check_connection]
                }

                callback.invoke(result)
            }
        })
    }
}