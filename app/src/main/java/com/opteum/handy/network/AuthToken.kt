package com.opteum.handy.network

import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppPrefs

class AuthToken {
    private val prefs: AppPrefs = App.kodein.instance()
    private var currentToken = prefs.get(R.string.pref_auth_token)

    fun getCurrentToken(): String {
        return currentToken ?: ""
    }

    fun setCurrentToken(token: String) {
        currentToken = token
    }
}