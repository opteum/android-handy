package com.opteum.handy.jni

object JniHandy {
    init {
        System.loadLibrary("handy_native")
    }

    external fun gett0resIdOfferFragment(strings: Array<String>): String?
    external fun gett0resIdLoadingLayout(strings: Array<String>): String?
    external fun gett0resIdAuthField(strings: Array<String>): String?
    external fun gett0resIdAuthLoadingMsg(strings: Array<String>): String?
    external fun gett0resIdSwitcher(strings: Array<String>): String?
    external fun gett0resIdRideButton(strings: Array<String>): String?
    external fun gett0resIdMsgDialog(strings: Array<String>): String?
    external fun gett0resIdBookingLayout(strings: Array<String>): String?
    external fun gett0resIdBookingOrderInfoLayout(strings: Array<String>): String?
    external fun gett0resIdDriverToolbar(strings: Array<String>): String?
    external fun gett0resIdToolbar(strings: Array<String>): String?
    external fun gett0resIdHistoryOrderTitle(strings: Array<String>): String?
    external fun gett0resIdAssignedOrder(strings: Array<String>): String?
    external fun gett0resIdInfoDialog(strings: Array<String>): String?
    external fun gett0resIdQuestionDialog(strings: Array<String>): String?
    external fun gett0resIdStats(strings: Array<String>): String?

    external fun opteum0resIdLoginField(strings: Array<String>): String?
    external fun opteum0resIdStartViewBtn(strings: Array<String>): String?
    external fun opteum0resIdSwitcher(strings: Array<String>): String?
    external fun opteum0resIdBtnYes(strings: Array<String>): String?
    external fun opteum0resIdBtnNo(strings: Array<String>): String?
    external fun opteum0resIdOffersCancelBtn(strings: Array<String>): String?
    external fun opteum0resIdLblBusy(strings: Array<String>): String?
    external fun opteum0resIdDriverAvatar(strings: Array<String>): String?
    external fun opteum0resIdMessages(strings: Array<String>): String?
    external fun opteum0resIdWorking(strings: Array<String>): String?
    external fun opteum0resIdStatusBtn(strings: Array<String>): String?

    external fun uber0resIdLauncherLayout(strings: Array<String>): String?
    external fun uber0resIdBtnOrderRide(strings: Array<String>): String?
    external fun uber0resIdSwitcher(strings: Array<String>): String?
    external fun uber0resIdOfferLayout(strings: Array<String>): String?
    external fun uber0resIdEarnings(strings: Array<String>): String?
    external fun uber0resIdAlert(strings: Array<String>): String?

    external fun yandex0resIdRideButtonOk(strings: Array<String>): String?
    external fun yandex0resIdRatingFeedback(strings: Array<String>): String?
    external fun yandex0resIdGridButtonText(strings: Array<String>): String?
    external fun yandex0resIdBtnHome(strings: Array<String>): String?
    external fun yandex0resIdBtnYes(strings: Array<String>): String?
    external fun yandex0resIdProgressIndicator(strings: Array<String>): String?
    external fun yandex0resIdProgressMessage(strings: Array<String>): String?
    external fun yandex0resIdAuthContent(strings: Array<String>): String?
    external fun yandex0resIdAuthPhone(strings: Array<String>): String?
    external fun yandex0resIdAuthParkBtn(strings: Array<String>): String?
    external fun yandex0resIdMessage(strings: Array<String>): String?
    external fun yandex0resIdSwitcher(strings: Array<String>): String?
    external fun yandex0resIdRequest(strings: Array<String>): String?
    external fun yandex0resIdRequestView(strings: Array<String>): String?
    external fun yandex0resIdChooseLanguage(strings: Array<String>): String?
    external fun yandex0resIdDriverBtn(strings: Array<String>): String?

    external fun taxsee0resIdOrderContainer(strings: Array<String>): String?
    external fun taxsee0resIdActionBar(strings: Array<String>): String?
    external fun taxsee0resIdActionTitle(strings: Array<String>): String?
    external fun taxsee0resIdCheckbox(strings: Array<String>): String?
    external fun taxsee0resIdCheckboxDbg(strings: Array<String>): String?
    external fun taxsee0resIdAlertTitle(strings: Array<String>): String?
    external fun taxsee0resIdLogin(strings: Array<String>): String?
    external fun taxsee0resIdLoading(strings: Array<String>): String?
    external fun taxsee0resIdHomeBtn(strings: Array<String>): String?
    external fun taxsee0resIdActionBtn(strings: Array<String>): String?
    external fun taxsee0resIdChannels(strings: Array<String>): String?

    external fun lyft0resIdBtnToggle(strings: Array<String>): String?
    external fun lyft0resIdDriverRideContainer(strings: Array<String>): String?
    external fun lyft0valueBtnToggleOnline(strings: Array<String>): String?
    external fun lyft0valueBtnToggleLastRide(strings: Array<String>): String?
    external fun lyft0resIdAcceptOrder(strings: Array<String>): String?

    external fun citymobil0resIdBtnSetBusy(strings: Array<String>): String?
    external fun citymobil0resIdBtnPositive(strings: Array<String>): String?
    external fun citymobil0resIdBtnOfferAccept(strings: Array<String>): String?
    external fun citymobil0resIdBtnArrived(strings: Array<String>): String?
    external fun citymobil0resIdBtnGo(strings: Array<String>): String?
    external fun citymobil0resIdBtnDone(strings: Array<String>): String?
    external fun citymobil0resIdDialogMessage(strings: Array<String>): String?
    external fun citymobil0resIdSplash(strings: Array<String>): String?
    external fun citymobil0resIdRegister(strings: Array<String>): String?
}

fun ((strings: Array<String>) -> String?).isIt(text: String): Boolean = this.invoke(kotlin.arrayOf(text)) != null