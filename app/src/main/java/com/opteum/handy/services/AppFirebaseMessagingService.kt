package com.opteum.handy.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.opteum.handy.R
import com.opteum.handy.Starter
import com.opteum.handy.utils.AppLog

class AppFirebaseMessagingService : FirebaseMessagingService() {
    private val channelIdDefault = "default"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        AppLog.log("fcm message received")

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            val title = it.title ?: return@let
            val body = it.body ?: return@let
            sendNotification(title, body)
        }
    }

    private fun sendNotification(title: String, messageBody: String) {
        val intent = packageManager.getLaunchIntentForPackage(packageName).run {
            val i = Intent(Intent.ACTION_MAIN)
            i.component = ComponentName(packageName, this.component.className)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.addCategory(Intent.CATEGORY_LAUNCHER)
            return@run i
        }

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelIdDefault)
                .setSmallIcon(R.drawable.ic_car)
                .setContentTitle(title)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            oreoNotification(notificationManager)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun oreoNotification(notificationManager: NotificationManager) {
        val channel = NotificationChannel(channelIdDefault, channelIdDefault, NotificationManager.IMPORTANCE_DEFAULT)
        notificationManager.createNotificationChannel(channel)
    }
}