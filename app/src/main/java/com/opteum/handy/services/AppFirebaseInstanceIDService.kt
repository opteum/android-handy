package com.opteum.handy.services

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.opteum.handy.metrics.YaHelper
import com.opteum.handy.mvp.Interactor_Fcm
import com.opteum.handy.utils.AppLog

class AppFirebaseInstanceIDService : FirebaseInstanceIdService() {
    private val interactor = Interactor_Fcm()

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        AppLog.log("onTokenRefresh: token = $refreshedToken")

        refreshedToken ?: yaNullToken()

        sendRegistrationToServer()
    }

    private fun sendRegistrationToServer() {
        interactor.sendToken({ if (!it.isSuccess) yaTokenSendFail() })
    }

    private fun yaNullToken() {
        YaHelper.reportProblem("Fcm token is null. An empty string has been sent.")
    }

    private fun yaTokenSendFail() {
        YaHelper.reportProblem("Can't successfully send fcm token.")
    }
}