package com.opteum.handy.services

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.AccessibilityServiceInfo
import android.accessibilityservice.GestureDescription
import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Handler
import android.view.accessibility.AccessibilityEvent
import com.opteum.handy.business_logic.TaxiHelper
import com.opteum.handy.metrics.YaHelper
import com.opteum.handy.mvp.Presenter_Controls


class HandyAccessibilityService : AccessibilityService() {
    private val receiver = buildReceiver()
    private val taxiHelper = TaxiHelper(this::performGlobalAction, this::performGesture)

    override fun onCreate() {
        super.onCreate()
        registerReceiver(receiver, IntentFilter(Presenter_Controls.INTENT_ACTION_APPS_CONFIG_CHANGED))
        registerReceiver(receiver, IntentFilter(Presenter_Controls.INTENT_ACTION_CURRENT_APPS_TO_BUSY))
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()
    }

    override fun onAccessibilityEvent(accessibilityEvent: AccessibilityEvent) {
        taxiHelper.onAccessibilityEvent(accessibilityEvent, rootInActiveWindow)
    }

    private fun buildReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                // todo receiver to helper?
                when (intent.action) {
                    Presenter_Controls.INTENT_ACTION_APPS_CONFIG_CHANGED -> taxiHelper.onAppsConfigChanged()
                    Presenter_Controls.INTENT_ACTION_CURRENT_APPS_TO_BUSY -> taxiHelper.onCurrentAppsToBusyCommand()
                }
            }
        }
    }

    override fun onServiceConnected() {
        serviceInfo.apply { flags = flags or AccessibilityServiceInfo.FLAG_REPORT_VIEW_IDS or AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS }.let { serviceInfo = it }
    }

    override fun onUnbind(intent: Intent?): Boolean {
        YaHelper.reportInfo("AccessibilityService был выключен")
        taxiHelper.onAccessibilityUnbound()
        return super.onUnbind(intent)
    }

    override fun onInterrupt() {}

    @TargetApi(Build.VERSION_CODES.N)
    private fun performGesture(gesture: GestureDescription, callback: AccessibilityService.GestureResultCallback?, handler: Handler?): Boolean {
        return dispatchGesture(gesture, callback, handler)
    }
}