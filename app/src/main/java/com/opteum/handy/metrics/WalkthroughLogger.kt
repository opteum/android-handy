package com.opteum.handy.metrics

import android.util.Log
import android.view.accessibility.AccessibilityEvent
import com.opteum.handy.BuildConfig
import com.opteum.handy.mvp.Interactor_Log
import java.util.*
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit


class WalkthroughLogger : ITaxiEventsRecorder {
    companion object {
        private const val MAX_EVENTS_TO_SEND = 100
        private const val MIN_SEND_INTERVAL_MS = 10000L
    }

    private val executor = ScheduledThreadPoolExecutor(1)

    private val interactorLog = Interactor_Log()
    private val events = ArrayList<EventPack.EventItem>()
    private var lastSendMs = 0L
    private var isIncludeContentChangedTypeEventsOnNextSend = false

    private val sendTask = { popAndSendEventsPack() }
    private var sendTaskFuture: ScheduledFuture<*>? = null

    fun log(eventType: Int, eventClassName: CharSequence, rootPackage: CharSequence, nodesInfo: String) {
        if (BuildConfig.DEBUG) {
            Log.d("lll", "$eventClassName\n$nodesInfo")
            return
        }

        try {
            collectEventInfo(rootPackage.toString(), eventClassName.toString(), System.currentTimeMillis(), nodesInfo, eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED)
        } catch (e: Exception) {
            YaHelper.reportError("Что-то не удалось залогировать", e)
        }
    }

    override fun logCustom(packageName: String, info: String) {
        if (BuildConfig.DEBUG) {
            Log.d("ddd", "$packageName: $info")
            return
        }

        events.add(EventPack.EventItem(System.currentTimeMillis(), packageName, "CUSTOM LOG", info, false))
    }

    override fun markTaskFail(packageName: String) {
        if (BuildConfig.DEBUG) return

        isIncludeContentChangedTypeEventsOnNextSend = true
    }

    private fun collectEventInfo(packageName: String, eventForm: String, timeMs: Long, nodesInfo: String, isContentChangedType: Boolean) {
        events.add(EventPack.EventItem(timeMs, packageName, eventForm, nodesInfo, isContentChangedType))

        if (System.currentTimeMillis() - lastSendMs >= MIN_SEND_INTERVAL_MS) {
            lastSendMs = System.currentTimeMillis()

            sendTask.invoke()

            sendTaskFuture?.cancel(false)
            executor.purge()

            sendTaskFuture = executor.schedule(sendTask, MIN_SEND_INTERVAL_MS, TimeUnit.MILLISECONDS)
        }
    }

    @Synchronized
    private fun popAndSendEventsPack() {
        var eventsToSend = if (isIncludeContentChangedTypeEventsOnNextSend) events else events.filter { !it.isContentChangedType }
        if (eventsToSend.size > MAX_EVENTS_TO_SEND) {
            YaHelper.reportError("Слишком много событий для отправки. Такого, в целом, не должно быть.", Exception())
            eventsToSend = eventsToSend.takeLast(MAX_EVENTS_TO_SEND)
        }

        interactorLog.log(eventsToSend)
        isIncludeContentChangedTypeEventsOnNextSend = false
        events.clear()
    }

    data class EventPack(val userId: String, val appVersionName: String, val events: List<EventItem>) {
        data class EventItem(val timeMs: Long, val packageName: String, val form: String, val nodesInfo: String, val isContentChangedType: Boolean)
    }
}