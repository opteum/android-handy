package com.opteum.handy.metrics

interface ITaxiEventsRecorder {
    fun logCustom(packageName: String, info: String)
    fun markTaskFail(packageName: String)
}