package com.opteum.handy.metrics

import com.opteum.handy.BuildConfig
import com.opteum.handy.models.Data_TaxiApp
import com.yandex.metrica.YandexMetrica
import org.json.JSONException
import org.json.JSONObject

object YaHelper {
    fun reportBusySuccess(appId: String, isFail: Boolean?) {
        if (BuildConfig.DEBUG) return

        try {
            YandexMetrica.reportEvent(
                    "ПЕРЕКЛЮЧЕНИЕ НА ЗАНЯТ",
                    JSONObject().put(
                            appId,
                            isFail?.let { if (!it) "SUCCESS" else "FAIL" } ?: "NULL")
                            .toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun reportCheckedAppsOnEnabled(apps: ArrayList<Data_TaxiApp>) {
        if (BuildConfig.DEBUG) return

        apps.forEach {
            if (it.isEnabled) {
                try {
                    YandexMetrica.reportEvent(
                            "ОТСЛЕЖИВАЕМОЕ ПРИЛОЖЕНИЕ ПРИ ВКЛЮЧЕНИИ",
                            JSONObject().put(
                                    it.id,
                                    "")
                                    .toString())
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun reportError(text: String, exception: Exception) {
        if (BuildConfig.DEBUG) return
        YandexMetrica.reportError(text, exception)
    }

    fun reportInfo(text: String) {
        if (BuildConfig.DEBUG) return

        try {
            YandexMetrica.reportEvent(
                    "ИНФОРМАЦИЯ",
                    JSONObject().put(
                            text,
                            "")
                            .toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun reportProblem(text: String) {
        if (BuildConfig.DEBUG) return

        try {
            YandexMetrica.reportEvent(
                    "ПРОБЛЕМА",
                    JSONObject().put(
                            text,
                            "")
                            .toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun reportIsHandy(isHandy: Boolean) {
        if (BuildConfig.DEBUG) return

        try {
            YandexMetrica.reportEvent(
                    "ОПРОС: ПОМОГАЕТ ПРИЛОЖЕНИЕ?",
                    JSONObject().put(
                            if (isHandy) "ДА" else "НЕТ",
                            "")
                            .toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun reportPaymentRejectReason(reasonText: String, isCustom: Boolean = false) {
        if (BuildConfig.DEBUG) return

        val title = if (!isCustom) reasonText else "ДРУГАЯ"
        val value = if (!isCustom) "" else reasonText

        try {
            YandexMetrica.reportEvent(
                    "ПРИЧИНА ОТКАЗА ОТ ОПЛАТЫ",
                    JSONObject().put(
                            title,
                            value)
                            .toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}