package com.opteum.handy.ui.base

import android.annotation.SuppressLint
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import com.opteum.handy.R
import com.opteum.handy.mvp.base.IView

@SuppressLint("Registered")
open class AppActivity : AppCompatActivity(), IView {
    private var snackbarProgress: Snackbar? = null
    private var snackbarMsg: Snackbar? = null
    private var isBlockTouch = false

    private fun root(): View {
        return findViewById(android.R.id.content)
    }

    fun setIsBlockTouch(block: Boolean) {
        this.isBlockTouch = block
    }

    override fun showProgress() {
        if (snackbarProgress == null) {
            snackbarProgress = Snackbar.make(root(), R.string.text_processing, Snackbar.LENGTH_INDEFINITE)
        }

        isBlockTouch = true
        snackbarProgress?.show()
    }

    override fun hideProgress() {
        isBlockTouch = false
        snackbarProgress?.dismiss()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (isBlockTouch) return false
        return super.dispatchTouchEvent(ev)
    }

    override fun showMessage(resMsg: Int) {
        showMessage(getString(resMsg))
    }

    override fun showMessage(msg: String) {
        snackbarMsg?.run { this.dismiss() }
        snackbarMsg = Snackbar.make(root(), msg, Snackbar.LENGTH_INDEFINITE)
        snackbarMsg?.view?.findViewById<TextView>(android.support.design.R.id.snackbar_text)?.maxLines = resources.getInteger(R.integer.snackbar_lines)
        snackbarMsg?.let { it.setAction(R.string.text_hide, { snackbarMsg?.dismiss() }) }
        snackbarMsg?.show()
    }

    override fun say(resMsg: Int) {
        toast(resMsg)
    }

    override fun say(msg: String) {
        toast(msg)
    }
}