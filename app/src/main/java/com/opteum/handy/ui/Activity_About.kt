package com.opteum.handy.ui

import android.os.Bundle
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.opteum.handy.BuildConfig
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_About
import com.opteum.handy.mvp.Presenter_About
import com.opteum.handy.ui.base.AppActivity
import com.opteum.handy.utils.AppsUtil
import kotlinx.android.synthetic.main.view_about.*

class Activity_About : AppActivity(), IView_About {
    private lateinit var presenter: Presenter_About

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_about)
        title = getText(R.string.text_about)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        init()
        presenter = Presenter_About()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    private fun init() {
        btnRateApp.setOnClickListener { presenter.onSendRateClick(rbRating.rating) }
        btnFeedback.setOnClickListener { presenter.onFeedbackClick() }
    }

    override fun showData() {
        tvVersion.text = getString(R.string.text_ph_version_and_value, BuildConfig.VERSION_NAME)
    }

    override fun rateOnStore() {
        AppsUtil.rateThisAppOnGooglePlay(this)
    }

    override fun showFeedbackDialog() {
        MaterialDialog.Builder(this)
                .title(R.string.text_any_suggestions)
                .content(getString(R.string.msg_dont_forget_contact_info))
                .negativeText(R.string.text_cancel)
                .input(getString(R.string.text_your_comment), null, { dialog, input ->
                    presenter.sendFeedback(input.toString())
                    dialog.dismiss()
                })
                .canceledOnTouchOutside(false)
                .show()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}