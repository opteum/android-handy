package com.opteum.handy.ui

import android.os.Bundle
import android.view.MenuItem
import com.opteum.handy.BuildConfig
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_FeedbackRequest
import com.opteum.handy.mvp.Presenter_FeedbackRequest
import com.opteum.handy.ui.base.AppActivity
import com.opteum.handy.utils.AppsUtil
import kotlinx.android.synthetic.main.activity_feedback_request.*

class Activity_FeedbackRequest : AppActivity(), IView_FeedbackRequest {
    private lateinit var presenter: Presenter_FeedbackRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback_request)
        title = getString(R.string.text_feedback)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        init()
        presenter = Presenter_FeedbackRequest()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    private fun init() {
        btnSend.setOnClickListener { presenter.sendFeedback(etFeedback.text.toString(), rbRating.rating) }
    }

    override fun showData() {
        val textVersion = getString(R.string.text_ph_version_and_value, BuildConfig.VERSION_NAME)
        tvVersion.text = textVersion
    }

    override fun close() {
        finish()
    }

    override fun rateOnStore() {
        AppsUtil.rateThisAppOnGooglePlay(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                presenter.onCloseClick()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}