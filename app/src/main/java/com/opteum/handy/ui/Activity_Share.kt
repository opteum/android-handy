package com.opteum.handy.ui

import android.os.Bundle
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_Share
import com.opteum.handy.mvp.Presenter_Share
import com.opteum.handy.ui.base.AppActivity
import kotlinx.android.synthetic.main.activity_share.*

class Activity_Share : AppActivity(), IView_Share {
    private lateinit var presenter: Presenter_Share

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share)

        init()
        presenter = Presenter_Share()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    private fun init() {
        btnOk.setOnClickListener { presenter.onOkClick() }
    }

    override fun close() {
        finish()
    }
}