package com.opteum.handy.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.opteum.handy.R
import com.opteum.handy.ui.base.AppFragment

class Fragment_Guide : AppFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_guide, container, false)
    }
}