package com.opteum.handy.ui.base

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import com.opteum.handy.R

class DividerItemDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private var mDivider: Drawable = ContextCompat.getDrawable(context, R.drawable.app_recycler_view_divider) ?: throw Exception("No drawable for divider")

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        // top divider
        mDivider.setBounds(left, 0, right, mDivider.intrinsicHeight)
        mDivider.draw(c)

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + mDivider.intrinsicHeight

            mDivider.setBounds(left, top, right, bottom)
            mDivider.draw(c)
        }
    }
}