package com.opteum.handy.ui.base

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun Activity.toast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

fun Activity.toast(res: Int) {
    Toast.makeText(this, this.getString(res), Toast.LENGTH_LONG).show()
}

fun Fragment.toast(text: String) {
    Toast.makeText(this.context, text, Toast.LENGTH_LONG).show()
}

fun Fragment.toast(res: Int) {
    Toast.makeText(this.context, this.getString(res), Toast.LENGTH_LONG).show()
}

fun Double.formatAsMoney(): String {
    val otherSymbols = DecimalFormatSymbols(Locale.getDefault())
    otherSymbols.decimalSeparator = ','
    otherSymbols.groupingSeparator = ' '
    val df = DecimalFormat("0.00", otherSymbols)
    df.isGroupingUsed = true
    return df.format(this)
}

inline fun Any?.ifNull(action: () -> Unit) {
    if (this == null) {
        action.invoke()
    }
}

inline fun Boolean?.ifTrue(action: () -> Unit) {
    if (this != null && this) {
        action.invoke()
    }
}

inline fun Boolean?.ifFalse(action: () -> Unit) {
    if (this != null && !this) {
        action.invoke()
    }
}

fun Boolean?.toBin(): String {
    return if (this != null) {
        if (this) "1" else "0"
    } else {
        "-"
    }
}

fun Context.createReceiver(callback: (intent: Intent) -> Unit): BroadcastReceiver {
    return object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            callback(intent)
        }
    }
}

fun Fragment.isActivityClosed(): Boolean {
    return activity?.isFinishing == true || (activity as? AppCompatActivity)?.isDestroyed == true
}

fun Fragment.isFragmentClosed(): Boolean {
    return activity?.isFinishing == true || !isAdded || isRemoving || (activity as? AppCompatActivity)?.isDestroyed == true
}