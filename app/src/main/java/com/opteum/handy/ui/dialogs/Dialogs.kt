package com.opteum.handy.ui.dialogs

import android.content.Context
import android.content.Intent
import com.afollestad.materialdialogs.MaterialDialog
import com.opteum.handy.R
import com.opteum.handy.ui.Activity_Login
import com.opteum.handy.ui.Activity_Register

object Dialogs {
    fun showLoginOnRegisterDialog(context: Context, msgResId: Int? = null) {
        val b = MaterialDialog.Builder(context)
                .items(context.getString(R.string.text_login), context.getString(R.string.text_register))
                .itemsCallback { dialog, _, _, text ->
                    when (text) {
                        context.getString(R.string.text_login) -> {
                            context.startActivity(Intent(context, Activity_Login::class.java))
                        }
                        context.getString(R.string.text_register) -> {
                            context.startActivity(Intent(context, Activity_Register::class.java))
                        }
                    }

                    dialog.dismiss()
                }
        msgResId?.let { b.title(it) }
        b.show()
    }
}