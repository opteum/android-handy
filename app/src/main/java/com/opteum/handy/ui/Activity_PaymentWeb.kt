package com.opteum.handy.ui

import android.app.Activity
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.mvp.Interactor_Auth
import com.opteum.handy.ui.base.AppActivity
import com.opteum.handy.utils.AppUtil
import kotlinx.android.synthetic.main.activity_payment.*


class Activity_PaymentWeb : AppActivity() {
    companion object {
        val EK_COST = "EK_COST"
    }

    private val appUtil: AppUtil = App.kodein.instance()
    private val interactorAuth = Interactor_Auth()

    private var isPaymentSucceed = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getText(R.string.text_payment)

        init()
    }

    private fun init() {
        val authToken = interactorAuth.getToken()
        val sum = intent.getStringExtra(EK_COST)
        if (sum.isNullOrBlank()) {
            say(R.string.text_error_something_gone_wrong)
            closeView()
            return
        }

        wvPayment.settings.apply {
            javaScriptEnabled = true
            domStorageEnabled = true
            useWideViewPort = true
            loadWithOverviewMode = true
        }
        wvPayment.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                if (url.contains(getString(R.string.url_part_payment_success))) {
                    isPaymentSucceed = true
                }
            }
        }
        val url = getString(R.string.url_payment, appUtil.getDeviceId(), authToken, sum)
        wvPayment.loadUrl(url)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            closeView()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        closeView()
    }

    private fun closeView() {
        if (isPaymentSucceed) {
            isPaymentSucceed = false
            setResult(Activity.RESULT_OK)
        }

        finish()
    }
}