package com.opteum.handy.ui.base

import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.widget.TextView
import com.opteum.handy.R
import com.opteum.handy.mvp.base.IView

open class AppFragment : Fragment(), IView {
    private var snackbarProgress: Snackbar? = null
    private var snackbarMsg: Snackbar? = null

    override fun showProgress() {
        view?.let { v ->
            if (snackbarProgress == null) {
                snackbarProgress = Snackbar.make(v, R.string.text_processing, Snackbar.LENGTH_INDEFINITE)
            }

            (activity as? AppActivity)?.setIsBlockTouch(true)
            snackbarProgress?.show()
        }
    }

    override fun hideProgress() {
        (activity as? AppActivity)?.setIsBlockTouch(false)
        snackbarProgress?.dismiss()
    }

    override fun showMessage(resMsg: Int) {
        showMessage(getString(resMsg))
    }

    override fun showMessage(msg: String) {
        view?.let { v ->
            snackbarMsg?.run { this.dismiss() }
            snackbarMsg = Snackbar.make(v, msg, Snackbar.LENGTH_INDEFINITE)
            snackbarMsg?.view?.findViewById<TextView>(android.support.design.R.id.snackbar_text)?.maxLines = resources.getInteger(R.integer.snackbar_lines)
            snackbarMsg?.let { it.setAction(R.string.text_hide, { snackbarMsg?.dismiss() }) }
            snackbarMsg?.show()
        }
    }

    override fun say(resMsg: Int) {
        toast(resMsg)
    }

    override fun say(msg: String) {
        toast(msg)
    }
}