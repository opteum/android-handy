package com.opteum.handy.ui.base

import android.support.v4.app.Fragment

object FragmentChanger {
    fun setFragment(a: AppActivity, layout: Int, f: Fragment) {
        if (a.isFinishing || a.isDestroyed) {
            return
        }

        val transaction = a.supportFragmentManager.beginTransaction()
        transaction.replace(layout, f, f::class.java.simpleName)

        transaction.commitAllowingStateLoss()
    }
}
