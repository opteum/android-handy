package com.opteum.handy.ui

import android.os.Bundle
import android.view.MenuItem
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_Auth
import com.opteum.handy.mvp.Presenter_Auth
import com.opteum.handy.ui.base.AppActivity
import kotlinx.android.synthetic.main.fragment_register.*

class Activity_Register : AppActivity(), IView_Auth {
    private lateinit var presenter: Presenter_Auth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_register)
        title = getText(R.string.text_register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        init()

        presenter = Presenter_Auth()
    }

    private fun init() {
        btnRegister.setOnClickListener { presenter.onRegisterClick(etEmail.text.toString(), etPassword.text.toString()) }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    override fun close() {
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}