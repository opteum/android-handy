package com.opteum.handy.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_PaymentReject
import com.opteum.handy.mvp.Presenter_PaymentReject
import com.opteum.handy.ui.base.AppActivity
import kotlinx.android.synthetic.main.activity_payment_reject.*

class Activity_PaymentReject : AppActivity(), IView_PaymentReject {
    private lateinit var presenter: Presenter_PaymentReject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_reject)
        title = getText(R.string.text_reject)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        init()

        presenter = Presenter_PaymentReject()
    }

    private fun init() {
        btnReasonNotInteresting.setOnClickListener { presenter.onClickReasonNotInterested() }
        btnReasonTooExpensive.setOnClickListener { presenter.onClickReasonTooExpensive() }
        btnReasonUnconvincingly.setOnClickListener { presenter.onClickReasonUnconvincingly() }
        btnReasonAnother.setOnClickListener { presenter.onClickReasonAnother() }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    override fun showAnotherReasonDialog() {
        val b = MaterialDialog.Builder(this)
        b.content(R.string.text_please_set_reject_reason)
        b.inputType(InputType.TYPE_CLASS_TEXT)
        b.input(null, null, { _, input -> presenter.anotherReasonText(input.toString()) })
        b.show()
    }

    override fun close() {
        goToTrialView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            startActivity(Intent(this, Activity_PaymentInfo::class.java).apply { flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT })
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        goToTrialView()
    }

    private fun goToTrialView() {
        setResult(Activity.RESULT_OK)
        finish()
    }
}