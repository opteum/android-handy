package com.opteum.handy.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_PaymentInfo
import com.opteum.handy.mvp.Presenter_PaymentInfo
import com.opteum.handy.ui.base.AppActivity
import com.opteum.handy.ui.dialogs.Dialogs
import com.opteum.handy.utils.LocaleUtil
import kotlinx.android.synthetic.main.activity_payment_view.*
import java.text.SimpleDateFormat
import java.util.*

class Activity_PaymentInfo : AppActivity(), IView_PaymentInfo {
    companion object {
        val RC_CLOSE_THIS = 999
        val EK_HIDE_REJECT_BTN = "EK_HIDE_REJECT_BTN"
    }

    private lateinit var presenter: Presenter_PaymentInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_view)
        title = getText(R.string.text_payment)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        init()

        presenter = Presenter_PaymentInfo()
    }

    private fun init() {
        intent?.getStringExtra(EK_HIDE_REJECT_BTN)?.let { btnReject.visibility = View.INVISIBLE }

        btnPay.setOnClickListener { presenter.onPayClick() }
        btnReject.setOnClickListener { presenter.onRejectClick() }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    @SuppressLint("SetTextI18n")
    override fun showData(header: String, description: String, cost: Int, expiredDateMs: Long) {
        tvHeader.text = header
        tvDescription.text = description
        tvPayment.text = "$cost ${Currency.getInstance(LocaleUtil.getRusLocale()).symbol}"
        if (expiredDateMs != 0L) {
            tvPaidUntil.text = "${getText(R.string.text_paid_until)} ${SimpleDateFormat("dd.MM.yyyy", Locale.US).format(Date(expiredDateMs))}"
        }
    }

    override fun showLoginOnRegisterDialog() {
        Dialogs.showLoginOnRegisterDialog(this, R.string.text_login_or_register_before_payment)
    }

    override fun goToPayView(cost: String) {
        startActivityForResult(Intent(this, Activity_PaymentWeb::class.java).apply { putExtra(Activity_PaymentWeb.EK_COST, cost) }, RC_CLOSE_THIS)
    }

    override fun goToRejectView() {
        startActivityForResult(Intent(this, Activity_PaymentReject::class.java), RC_CLOSE_THIS)
    }

    override fun close() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_CLOSE_THIS && resultCode == Activity.RESULT_OK) {
            finish()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}