package com.opteum.handy.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_Settings
import com.opteum.handy.mvp.Presenter_Settings
import com.opteum.handy.ui.base.AppFragment
import com.opteum.handy.ui.dialogs.Dialogs
import kotlinx.android.synthetic.main.fragment_settings.*

class Fragment_Settings : AppFragment(), IView_Settings {
    private lateinit var presenter: Presenter_Settings

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
        presenter = Presenter_Settings()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    private fun init() {
        (activity as? AppCompatActivity)?.apply {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        btnPayment.setOnClickListener { presenter.onPaymentClick() }
        btnProfile.setOnClickListener { presenter.onProfileClick() }
        btnFeedback.setOnClickListener { presenter.onAboutClick() }
        btnAuthAction.setOnClickListener { presenter.onAuthActionClick() }
    }

    override fun invalidateAuthButtonsView(email: String?) {
        btnAuthAction.text = if (email != null) getString(R.string.ph_logout_email, email) else getString(R.string.text_login_or_register)
        btnProfile.visibility = if (email != null) View.VISIBLE else View.GONE
    }

    override fun openPaymentView() {
        startActivity(Intent(activity, Activity_PaymentInfo::class.java).apply { putExtra(Activity_PaymentInfo.EK_HIDE_REJECT_BTN, "") })
    }

    override fun openAboutView() {
        startActivity(Intent(activity, Activity_About::class.java))
    }

    override fun openProfileView() {
        startActivity(Intent(activity, Activity_Profile::class.java))
    }

    override fun showLoginOnRegisterDialog() {
        context?.let { Dialogs.showLoginOnRegisterDialog(it) }
    }
}