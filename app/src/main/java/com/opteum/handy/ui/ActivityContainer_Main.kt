package com.opteum.handy.ui

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.MenuItem
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.Starter
import com.opteum.handy.mvp.IView_Main
import com.opteum.handy.mvp.Presenter_Controls
import com.opteum.handy.mvp.Presenter_Main
import com.opteum.handy.ui.base.AppActivity
import com.opteum.handy.ui.base.FragmentChanger
import com.opteum.handy.ui.base.createReceiver
import com.opteum.handy.ui.base.toast
import kotlinx.android.synthetic.main.activity_main.*

class ActivityContainer_Main : AppActivity(), IView_Main {
    private lateinit var presenter: Presenter_Main
    private val fragmentChanger: FragmentChanger = App.kodein.instance()
    private val receiver = createReceiver(this::onReceive)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        presenter = Presenter_Main()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
        presenter.onUserHere()

        registerReceiver(receiver, IntentFilter(Presenter_Controls.INTENT_ACTION_APPS_CONFIG_CHANGED))
    }

    override fun onPause() {
        unregisterReceiver(receiver)

        presenter.onStop()
        super.onPause()
    }

    private fun init() {
        if (supportFragmentManager.fragments.size == 0) {
            setFirstFragment()
        }

        bottomNav.setOnNavigationItemSelectedListener(this::navTo)
    }

    private fun setFirstFragment() {
        fragmentChanger.setFragment(this, R.id.container, Fragment_Controls())
        bottomNav.selectedItemId = R.id.app_bar_controls
        toast(R.string.text_dont_forget_to_read_a_guide)
    }

    private fun navTo(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.app_bar_controls -> {
                fragmentChanger.setFragment(this, R.id.container, Fragment_Controls())
                true
            }
            R.id.app_bar_guide -> {
                fragmentChanger.setFragment(this, R.id.container, Fragment_Guide())
                true
            }
            R.id.app_bar_settings -> {
                fragmentChanger.setFragment(this, R.id.container, Fragment_Settings())
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    private fun onReceive(intent: Intent) {
        if (intent.action == Presenter_Controls.INTENT_ACTION_APPS_CONFIG_CHANGED) {
            presenter.onAppConfigChanged()
        }
    }

    override fun showFeedbackRequestView() {
        startActivity(Intent(this, Activity_FeedbackRequest::class.java))
    }

    override fun showIsHandyRequestView() {
        startActivity(Intent(this, Activity_IsHandy::class.java))
    }

    override fun navToStart() {
        finish()
        startActivity(Intent(this, Starter::class.java))
    }
}
