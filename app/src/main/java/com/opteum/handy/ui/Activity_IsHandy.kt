package com.opteum.handy.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.metrics.YaHelper
import com.opteum.handy.mvp.IView_IsHandy
import com.opteum.handy.mvp.Presenter_IsHandy
import com.opteum.handy.ui.base.AppActivity
import kotlinx.android.synthetic.main.activity_is_handy.*

class Activity_IsHandy : AppActivity(), IView_IsHandy {
    private lateinit var presenter: Presenter_IsHandy
    private val ctx: Context = App.kodein.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_is_handy)

        init()
        presenter = Presenter_IsHandy()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    private fun init() {
        btnYes.setOnClickListener { presenter.onYesClick() }
        btnNo.setOnClickListener { presenter.onNoClick() }
    }

    override fun goToTellAbout() {
        YaHelper.reportIsHandy(true)
        startActivity(Intent(this, Activity_Share::class.java))
        finish()
    }

    override fun goToFeedback() {
        YaHelper.reportIsHandy(false)
        Toast.makeText(ctx, getString(R.string.text_describe_your_problem_please), Toast.LENGTH_LONG).show()
        startActivity(Intent(this, Activity_FeedbackRequest::class.java))
        finish()
    }
}