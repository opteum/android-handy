package com.opteum.handy.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.opteum.handy.R
import com.opteum.handy.models.Data_TaxiApp

class Adapter_TaxiApps(private val items: ArrayList<Data_TaxiApp>) : RecyclerView.Adapter<Adapter_TaxiApps.VH>() {
    private var clickCallback: ((item: Data_TaxiApp) -> Unit)? = null
    private var isSleepMode = true

    fun setClickCallback(callback: ((item: Data_TaxiApp) -> Unit)) {
        this.clickCallback = callback
    }

    fun setSleepMode(isSleep: Boolean) {
        this.isSleepMode = isSleep
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_taxi_app, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = items[position]

        holder.ivTaxiIcon.setImageResource(if (item.isEnabled && !isSleepMode) item.iconResEnabled else item.iconResDisabled)

        if (item.isEnabled) {
            holder.ivTaxiIsEnabled.setImageResource(if (isSleepMode) R.drawable.ic_checked_sleep else R.drawable.ic_checked_active)
        } else {
            holder.ivTaxiIsEnabled.setImageResource(R.drawable.app_circle_gray)
        }

        holder.tvTaxiName.text = item.name

        holder.root.setOnClickListener { clickCallback?.invoke(item) }
    }

    class VH(var view: View) : RecyclerView.ViewHolder(view) {
        var root: View = view
        var ivTaxiIcon: ImageView = view.findViewById(R.id.ivTaxiIcon)
        var tvTaxiName: TextView = view.findViewById(R.id.tvTaxiName)
        var ivTaxiIsEnabled: ImageView = view.findViewById(R.id.ivTaxiIsEnabled)
    }
}