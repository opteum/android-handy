package com.opteum.handy.ui

import android.os.Bundle
import android.view.MenuItem
import com.opteum.handy.R
import com.opteum.handy.mvp.IView_Profile
import com.opteum.handy.mvp.Presenter_Profile
import com.opteum.handy.network.responses.Response_Profile
import com.opteum.handy.ui.base.AppActivity
import kotlinx.android.synthetic.main.fragment_profile.*

class Activity_Profile : AppActivity(), IView_Profile {
    private lateinit var presenter: Presenter_Profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_profile)
        title = getText(R.string.text_profile)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        init()

        presenter = Presenter_Profile()
    }

    private fun init() {
        btnSave.setOnClickListener { presenter.onSaveClick(etPhone.text.toString(), etName.text.toString()) }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    override fun showData(data: Response_Profile) {
        etPhone.setText(data.phone)
        etName.setText(data.name)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}