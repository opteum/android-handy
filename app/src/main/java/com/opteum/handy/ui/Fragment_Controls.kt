package com.opteum.handy.ui

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.opteum.handy.R
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.mvp.IView_Controls
import com.opteum.handy.mvp.Interactor_Controls
import com.opteum.handy.mvp.Presenter_Controls
import com.opteum.handy.ui.adapters.Adapter_TaxiApps
import com.opteum.handy.ui.base.AppFragment
import com.opteum.handy.ui.base.DividerItemDecoration
import com.opteum.handy.ui.base.isFragmentClosed
import kotlinx.android.synthetic.main.fragment_controls.*
import java.util.*

// todo make permanent service check?
class Fragment_Controls : AppFragment(), IView_Controls {
    private lateinit var presenter: Presenter_Controls
    private val interactorControls = Interactor_Controls()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_controls, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
        presenter = Presenter_Controls()
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    private fun init() {
        // todo is it ok?
        (activity as? AppCompatActivity)?.apply {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }
        setHasOptionsMenu(true)

        scModeSwitcher.setOnCheckedChangeListener { _, isChecked -> presenter.onChangeIsEnabled(isChecked) }
    }

    private fun changeVisualOfEnabled(isEnabled: Boolean) {
        val ctx = context ?: return

        if (isEnabled) {
            if (!scModeSwitcher.isChecked) scModeSwitcher.isChecked = true
            val startColor = ContextCompat.getColor(ctx, R.color.colorGray)
            val targetColor = ContextCompat.getColor(ctx, R.color.colorBlue)
            tvToolbarTitle.setTextColor(ContextCompat.getColor(ctx, android.R.color.white))
            toolbar.overflowIcon = ContextCompat.getDrawable(ctx, R.drawable.ic_menu_white_24dp)
            tvToolbarTitle.text = getText(R.string.text_online)
            toggleActionBarColor(startColor, targetColor)
        } else {
            if (scModeSwitcher.isChecked) scModeSwitcher.isChecked = false
            val startColor = ContextCompat.getColor(ctx, R.color.colorBlue)
            val targetColor = ContextCompat.getColor(ctx, R.color.colorGray)
            tvToolbarTitle.text = getText(R.string.text_offline)
            toolbar.overflowIcon = ContextCompat.getDrawable(ctx, R.drawable.ic_menu_black_24dp)
            tvToolbarTitle.setTextColor(ContextCompat.getColor(ctx, android.R.color.black))
            toggleActionBarColor(startColor, targetColor)
        }
    }

    private fun toggleActionBarColor(originalColor: Int, targetColor: Int) {
        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), originalColor, targetColor)
        colorAnimation.duration = resources.getInteger(R.integer.switcher_duration_ms) / 2L
        colorAnimation.addUpdateListener { animator -> activity?.toolbar?.setBackgroundColor(animator.animatedValue as Int) }
        colorAnimation.start()
    }

    override fun setInitDataForView(isEnabled: Boolean, taxiApps: ArrayList<Data_TaxiApp>) {
        if (isFragmentClosed()) return

        val ctx = context ?: return

        if (taxiApps.size == 0) {
            rvTaxiApps.visibility = View.GONE
            llNoData.visibility = View.VISIBLE
        }

        rvTaxiApps.addItemDecoration(DividerItemDecoration(ctx))
        rvTaxiApps.layoutManager = LinearLayoutManager(context)
        rvTaxiApps.adapter = Adapter_TaxiApps(taxiApps).apply { setSleepMode(!isEnabled); setClickCallback { presenter.onTaxiAppClick(it) } }

        invalidateStateView(isEnabled, true)
    }

    override fun invalidateStateView(isEnabled: Boolean, isNoAnimation: Boolean) {
        if (isFragmentClosed()) return

        scModeSwitcher.isChecked = isEnabled

        changeVisualOfEnabled(isEnabled)

        (rvTaxiApps.adapter as? Adapter_TaxiApps)?.setSleepMode(!isEnabled)
        rvTaxiApps.adapter.notifyDataSetChanged()
    }

    override fun invalidateTtsView() {
        if (isFragmentClosed()) return
        activity?.invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
        menu?.findItem(R.id.app_bar_tts)?.isChecked = interactorControls.isTtsEnabled()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.app_bar_tts -> {
                presenter.onTtsClick()
                true
            }
            R.id.app_bar_manual_set_busy -> {
                presenter.setCurrentAppsToBusy()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}
