package com.opteum.handy.ui.base

class InteractorResult<T> {
    var data: T? = null
    var isSuccess: Boolean = false
    var errorMessage: String? = null
}