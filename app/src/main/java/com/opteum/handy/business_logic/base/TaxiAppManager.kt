package com.opteum.handy.business_logic.base

import android.content.Context
import android.os.Handler
import android.view.accessibility.AccessibilityEvent
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.features.Tts
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.metrics.YaHelper
import com.opteum.handy.mvp.Interactor_Controls
import com.opteum.handy.mvp.Interactor_Log
import com.opteum.handy.ui.base.ifNull
import com.opteum.handy.ui.base.ifTrue
import com.opteum.handy.utils.AppsUtil
import java.util.*
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


abstract class TaxiAppManager(private val taskDoneCallback: () -> Unit,
                              private val orderModeCallback: (appId: String, isOn: Boolean) -> Unit,
                              val appId: String, internal val appPackageName: String, private val appName: String,
                              private val taxiLogger: ITaxiEventsRecorder, val errorTimeoutMs: Long = 1000L) {

    companion object {
        const val LOG_INFO_AUTH_VIEW_DETECTED = "AUTH VIEW DETECTED"
        const val LOG_INFO_RIDE_VIEW_DETECTED = "RIDE VIEW DETECTED"
        const val LOG_INFO_SKIP_VIEW_DETECTED = "SKIP VIEW DETECTED"
        const val LOG_INFO_ASGN_VIEW_DETECTED = "ASGN VIEW DETECTED"
        const val LOG_INFO_OFFR_VIEW_DETECTED = "OFFR VIEW DETECTED"
    }

    private val interactorControls = Interactor_Controls()
    private val interactorLog = Interactor_Log()
    protected val ctx: Context = App.kodein.instance()
    private val tts: Tts = App.kodein.instance()
    private val handler = Handler(ctx.mainLooper)
    private val timerForCallbackDelay = Timer()
    protected var task = Task.UNSET
    private var isOrderOnProgress = false

    private val executor = ScheduledThreadPoolExecutor(1)
    private var onWentWrongTimeoutFuture: ScheduledFuture<*>? = null

    private var currentNodes = NodesList(HashMap(), ArrayList())

    fun onAccessibilityEvent(event: AccessibilityEvent, nodes: NodesList, isSomeOrderInProgress: Boolean) {
        currentNodes = nodes

        val isTaskExist = task != Task.UNSET
        val isDetectOrderState = !isSomeOrderInProgress || isOrderOnProgress

        if (isTaskExist) {
            taxiLogger.logCustom(appPackageName, "TRYING EXECUTE TASK - ${task.name} (${event.className})")
            onWentWrongTimeoutFuture.ifNull { startSequenceWentWrongCountdown(logInfo = "default timeout") }
            onNewViewContentIfAnyTask(event, nodes)
        } else if (isDetectOrderState) {
            detectOrderState(event, nodes)
        }
    }

    fun setNewTask(newTask: Task) {
        this.task = newTask
    }

    fun isHaveTask(): Boolean {
        return task != Task.UNSET
    }

    protected fun taskDone(isFail: Boolean? = false, info: String = "", callbackDelayMs: Long = 0L) {
        recycleCurrentNodes()

        val successText = isFail?.let { (!it).toString() } ?: isFail.toString()
        val infoText = if (info.isNotBlank()) "($info)" else ""
        taxiLogger.logCustom(appPackageName, "TASK DONE - OK = $successText $infoText")

        YaHelper.reportBusySuccess(appId, isFail)
        interactorLog.reportAppMadeItsWork(appId, {})

        isFail.ifTrue { taxiLogger.markTaskFail(appPackageName) }

        task = Task.UNSET
        onResetSequenceCase()
        cancelPendingWentWrongTask()

        if (callbackDelayMs <= 0) {
            taskDoneCallback.invoke()
        } else {
            timerForCallbackDelay.schedule(object : TimerTask() {
                override fun run() {
                    taskDoneCallback.invoke()
                }
            }, callbackDelayMs)
        }
    }

    protected fun taskFailNotifyAndDone(logInfo: String = "") {
        onResetSequenceCase()

        val say = "\"$appName\": ${ctx.getString(R.string.text_cant_execute_task)}"
        handler.post { Toast.makeText(ctx, say, Toast.LENGTH_LONG).show() }
        (interactorControls.isTtsEnabled()).ifTrue { tts.sayAdd(say) }

        taskDone(true, logInfo)
    }

    protected fun taskUndefinedResultNotifyAndDone(info: String = "", isTts: Boolean = false, say: String = "") {
        onResetSequenceCase()
        val toSay = if (say.isBlank()) ctx.getString(R.string.text_cant_execute_task) else say
        handler.post { Toast.makeText(ctx, "\"$appName\": $toSay", Toast.LENGTH_LONG).show() }
        (isTts && interactorControls.isTtsEnabled()).ifTrue { tts.sayAdd(toSay) }

        taskDone(null, info)
    }

    internal fun getIsOrderInProgress(): Boolean {
        return isOrderOnProgress
    }

    internal fun bringAppToFrontToExecuteTask() {
        taxiLogger.logCustom(appPackageName, "BRINGING TO FRONT - ${task.name} isOrder = $isOrderOnProgress")
        AppsUtil.startApp(ctx, appPackageName)
    }

    private fun setOrderInProgress(isInProgress: Boolean) {
        taxiLogger.logCustom(appPackageName, "SET ORDER - $isInProgress")

        task = Task.UNSET
        isOrderOnProgress = isInProgress
        orderModeCallback.invoke(appId, isInProgress)
    }

    private fun detectOrderState(event: AccessibilityEvent, nodes: NodesList) {
        if (!getIsOrderInProgress() && isOrderOpenedView(event, nodes)) {
            setOrderInProgress(true)
        } else if (getIsOrderInProgress() && isOrderClosedView(event, nodes)) {
            setOrderInProgress(false)
        }

        recycleCurrentNodes()
    }

    protected fun startSequenceWentWrongCountdown(timeoutMs: Long = errorTimeoutMs, isTts: Boolean = false, say: String = "", logInfo: String = "", undefinedTaskResult: Boolean = false) {
        cancelPendingWentWrongTask()

        val pendingWentWrongTask = if (undefinedTaskResult) {
            { taskUndefinedResultNotifyAndDone(logInfo, isTts, say) }
        } else {
            { taskFailNotifyAndDone(logInfo) }
        }

        onWentWrongTimeoutFuture = executor.schedule(pendingWentWrongTask, timeoutMs, TimeUnit.MILLISECONDS)
    }

    private fun cancelPendingWentWrongTask() {
        onWentWrongTimeoutFuture?.cancel(false)
        executor.purge()
    }

    protected fun getAuthViewDetectedMsg(): String {
        return String.format(ctx.getString(R.string.msg_err_app_taximeter_not_started), appName)
    }

    protected fun getRideViewDetectedMsg(): String {
        return String.format(ctx.getString(R.string.msg_err_ride_in_progress), appName)
    }

    protected fun getOfferViewDetectedMsg(): String {
        return String.format(ctx.getString(R.string.msg_err_offer_view_detected), appName)
    }

    protected fun getSkipViewDetectedMsg(): String {
        return String.format(ctx.getString(R.string.msg_err_skip_view_detected), appName)
    }

    protected fun getAssignedViewDetectedMsg(): String {
        return String.format(ctx.getString(R.string.msg_err_assigned_order_view_detected), appName)
    }

    protected fun say(resId: Int, isTts: Boolean) {
        handler.post { Toast.makeText(ctx, "${ctx.getString(resId)} \"$appName\"", Toast.LENGTH_LONG).show() }
        (isTts && interactorControls.isTtsEnabled()).ifTrue { tts.sayAdd(resId) }
    }

    private fun recycleCurrentNodes() {
        currentNodes.recycle()
    }

    open fun onResetSequenceCase() {}
    open fun onLeaveWhileTask() {}

    abstract fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList)
    abstract fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean
    abstract fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean

    enum class Task {
        UNSET,
        BUSY,
    }
}