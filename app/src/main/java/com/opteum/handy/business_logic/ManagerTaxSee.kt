package com.opteum.handy.business_logic

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.BuildConfig
import com.opteum.handy.R
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.ui.base.ifFalse

class ManagerTaxSee(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder, private val globalAction: (actionId: Int) -> Boolean) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_TAXSEE, Data_TaxiApp.PACKAGE_TAXSEE, Data_TaxiApp.NAME_TAXSEE, failsLog) {

    companion object {
        private const val APP_LOADING_MAX_TIME_MS = 20000L
        private const val DONE_DELAY_FOR_SWITCHER_CLICK = 250L
    }

    private val resIdOrderContainer = JniHandy::taxsee0resIdOrderContainer
    private val resIdActionBar = JniHandy::taxsee0resIdActionBar
    private val resIdActionTitle = JniHandy::taxsee0resIdActionTitle
    private val resIdCheckbox = if (!BuildConfig.DEBUG) JniHandy::taxsee0resIdCheckbox else JniHandy::taxsee0resIdCheckboxDbg
    private val resIdAlertTitle = JniHandy::taxsee0resIdAlertTitle
    private val resIdLogin = JniHandy::taxsee0resIdLogin
    private val resIdLoading = JniHandy::taxsee0resIdLoading
    private val resIdHomeBtn = JniHandy::taxsee0resIdHomeBtn
    private val resIdActionBtn = JniHandy::taxsee0resIdActionBtn
    private val resIdChannels = JniHandy::taxsee0resIdChannels

    private var isLoadWarn = false

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdOrderContainer] != null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        val nodeTitle = nodes[resIdActionTitle]
        return nodeTitle != null && Regex("\\(\\d+.+\\)").find(nodeTitle.text)?.value?.isNotBlank() ?: false
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleFreeOrBusyTask(event, nodes)
        }
    }

    private fun handleFreeOrBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        val menuBtn = nodes[resIdActionBar]?.let { if (it.childCount > 0) it.getChild(it.childCount - 1) else null }

        // optional // loading? lets wait a bit longer
        if (nodes[resIdLoading] != null && nodes[resIdLogin] == null) {
            isLoadWarn.ifFalse { say(R.string.msg_waiting_launching_done_or_start_in_advance, true); isLoadWarn = true }
            startSequenceWentWrongCountdown(APP_LOADING_MAX_TIME_MS)
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_RIDE_VIEW_DETECTED, true, getRideViewDetectedMsg())
        }
        // optional // question or info form. nothing to do
        else if (nodes[resIdAlertTitle] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // optional // chat view. skip it.
        else if (nodes[resIdChannels] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // optional // auth form. nothing to do
        else if (nodes[resIdLoading] != null || nodes[resIdLogin] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_AUTH_VIEW_DETECTED, true, getAuthViewDetectedMsg())
        }
        // optional // form has action button. mean offline state or ride. place the check at the end if possible.
        else if (nodes[resIdActionBtn] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // smart // close orders view
        else if (nodes[resIdHomeBtn] != null) {
            nodes[resIdHomeBtn]?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
        }
        // stage 1
        else if (menuBtn != null) {
            startSequenceWentWrongCountdown()

            menuBtn.performAction(AccessibilityNodeInfo.ACTION_CLICK)
        }
        // stage 2
        else if (nodes[resIdCheckbox] != null) {
            startSequenceWentWrongCountdown()

            nodes[resIdCheckbox]?.let {
                if (it.isCheckable) {
                    if (it.isChecked) {
                        it.parent?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                    } else {
                        globalAction.invoke(AccessibilityService.GLOBAL_ACTION_BACK)
                    }

                    taskDone(callbackDelayMs = DONE_DELAY_FOR_SWITCHER_CLICK)
                }
            }
        }
    }

    override fun onResetSequenceCase() {
        isLoadWarn = false
    }
}
