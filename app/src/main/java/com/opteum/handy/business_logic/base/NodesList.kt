package com.opteum.handy.business_logic.base

import android.view.accessibility.AccessibilityNodeInfo
import java.util.*

class NodesList(val map: HashMap<String, AccessibilityNodeInfo>, private val nodesIds: ArrayList<String>) {

    operator fun get(f: (strings: Array<String>) -> String?): AccessibilityNodeInfo? = map[f.invoke(nodesIds.toTypedArray()) ?: "*"]

    fun size(): Int = map.size

    fun recycle() {
        map.values.forEach { it.recycle() }
    }
}