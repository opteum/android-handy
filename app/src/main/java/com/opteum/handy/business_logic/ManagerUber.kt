package com.opteum.handy.business_logic

import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.R
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.ui.base.ifFalse

class ManagerUber(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_UBER, Data_TaxiApp.PACKAGE_UBER, Data_TaxiApp.NAME_UBER, failsLog) {

    companion object {
        private const val APP_LOADING_MAX_TIME_MS = 20000L
        private const val DONE_DELAY_FOR_SWITCHER_CLICK = 250L
    }

    private val resIdLauncherLayout = JniHandy::uber0resIdLauncherLayout
    private val resIdBtnOrderRide = JniHandy::uber0resIdBtnOrderRide
    private val resIdSwitcher = JniHandy::uber0resIdSwitcher
    private val resIdOfferLayout = JniHandy::uber0resIdOfferLayout
    private val resIdEarnings = JniHandy::uber0resIdEarnings
    private val resIdAlert = JniHandy::uber0resIdAlert

    private var isLoadWarn = false

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdBtnOrderRide] != null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdSwitcher] != null
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleBusyTask(event, nodes)
        }
    }

    private fun handleBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        val switcher = nodes[resIdSwitcher]

        // stage 1
        if (switcher != null) {
            startSequenceWentWrongCountdown()

            val isOnline = switcher.isChecked
            if (isOnline) {
                switcher.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            }

            taskDone(callbackDelayMs = DONE_DELAY_FOR_SWITCHER_CLICK)
        }
        // optional // loading? lets wait a bit longer
        else if (nodes[resIdLauncherLayout] != null) {
            isLoadWarn.ifFalse { say(R.string.msg_waiting_launching_done_or_start_in_advance, true); isLoadWarn = true }
            startSequenceWentWrongCountdown(APP_LOADING_MAX_TIME_MS)
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            startSequenceWentWrongCountdown(errorTimeoutMs, true, getRideViewDetectedMsg(), LOG_INFO_RIDE_VIEW_DETECTED, true)
        }
        // optional // offer form. nothing to do
        else if (nodes[resIdOfferLayout] != null) {
            startSequenceWentWrongCountdown(errorTimeoutMs, true, getOfferViewDetectedMsg(), LOG_INFO_OFFR_VIEW_DETECTED, true)
        }
        // optional // earnings form. nothing to do
        else if (nodes[resIdEarnings] != null) {
            startSequenceWentWrongCountdown(errorTimeoutMs, true, getSkipViewDetectedMsg(), LOG_INFO_SKIP_VIEW_DETECTED, true)
        }
        // optional // alert form. nothing to do
        else if (nodes[resIdAlert] != null) {
            startSequenceWentWrongCountdown(errorTimeoutMs, true, getSkipViewDetectedMsg(), LOG_INFO_SKIP_VIEW_DETECTED, true)
        }
    }

    override fun onResetSequenceCase() {
        isLoadWarn = false
    }
}