package com.opteum.handy.business_logic

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.content.Context
import android.os.Handler
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.business_logic.utils.NodesUtil
import com.opteum.handy.data.AppStrings
import com.opteum.handy.features.Tts
import com.opteum.handy.metrics.WalkthroughLogger
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.mvp.Interactor_Controls
import com.opteum.handy.utils.AppsUtil
import java.lang.StringBuilder


class TaxiHelper(private val globalAction: (actionId: Int) -> Boolean, private val dispatchGesture: (gesture: GestureDescription, callback: AccessibilityService.GestureResultCallback?, handler: Handler?) -> Boolean) {
    private val ctx: Context = App.kodein.instance()
    private val strings: AppStrings = App.kodein.instance()
    private val tts: Tts = App.kodein.instance()
    private val interactorControls = Interactor_Controls()
    private val enabledManagers = HashMap<String, TaxiAppManager>()
    private val walkLogger: WalkthroughLogger = App.kodein.instance()

    private var isOn = interactorControls.isEnabled()

    fun onAppsConfigChanged() {
        isOn = interactorControls.isEnabled()

        if (!isOn) {
            enabledManagers.clear()
        } else {
            val managersPrevious = HashMap<String, TaxiAppManager>(enabledManagers)
            val managersOfOnConfig = HashMap<String, TaxiAppManager>()
            interactorControls.getTaxiAppsCurrentStateList().forEach { appConfig ->
                if (appConfig.isEnabled) {
                    val manager = createManagerById(appConfig.id)
                    managersOfOnConfig[manager.appPackageName] = manager
                }
            }

            // remove old; add new; keep unchanged;
            enabledManagers.clear()
            managersOfOnConfig.keys.forEach { key ->
                val managerToAdd = if (managersPrevious[key] != null) managersPrevious[key] else managersOfOnConfig[key]
                managerToAdd?.let { value -> enabledManagers.put(key, value) }
            }
        }
    }

    fun onCurrentAppsToBusyCommand() {
        enabledManagers.values.forEach { it.setNewTask(TaxiAppManager.Task.BUSY) }
        doNextTask()
    }

    fun onAccessibilityEvent(event: AccessibilityEvent, rootInActiveWindow: AccessibilityNodeInfo?) {
        if (!isOn || rootInActiveWindow == null) {
            rootInActiveWindow?.recycle()
            return
        }

        val packageName = rootInActiveWindow.packageName

        // check somehow changed to non executable package
        getSortedEnabledManagers().firstOrNull { it.isHaveTask() }?.let { if (packageName != it.appPackageName) it.onLeaveWhileTask() }

        // check is taximeter package
        val isRequiredPackage = isRequiredPackage(packageName)
        if (!isRequiredPackage) {
            rootInActiveWindow.recycle()
            return
        }

        // check manager enabled
        val taxiAppManager = enabledManagers[packageName]
        if (taxiAppManager == null) {
            rootInActiveWindow.recycle()
            return
        }

        // get nodes and string
        val (nodes, nodesString) = getNodesAndString(rootInActiveWindow)

        // log event
        walkLogger.log(event.eventType, event.className, rootInActiveWindow.packageName, nodesString)

        // handle event by manager
        taxiAppManager.onAccessibilityEvent(event, nodes, enabledManagers.values.any { it.getIsOrderInProgress() })

        // recycle root node
        rootInActiveWindow.recycle()
    }

    private fun onOrderEvent(appId: String, isOn: Boolean) {
        if (isOn) {
            enabledManagers.values.forEach { if (it.appId != appId) it.setNewTask(TaxiAppManager.Task.BUSY) }
            doNextTask()
        } else {
            Toast.makeText(ctx, R.string.msg_order_finished_you_may_enable_apps, Toast.LENGTH_LONG).show()
            openControlsView()
        }
    }

    private fun doNextTask() {
        val managers = getSortedEnabledManagers()
        val managerWithTask = managers.firstOrNull { it.isHaveTask() }
        val managerInOrderMode = managers.firstOrNull { it.getIsOrderInProgress() }

        when {
            managerWithTask != null -> managerWithTask.bringAppToFrontToExecuteTask()
            managerInOrderMode != null -> managerInOrderMode.bringAppToFrontToExecuteTask()
        }
    }

    private fun getSortedEnabledManagers(): List<TaxiAppManager> {
        return enabledManagers.values.sortedBy { it.appId }
    }

    private fun openControlsView() {
        walkLogger.logCustom(ctx.packageName, "BACK TO CONTROLS")
        AppsUtil.resumeToThisApp(ctx)
    }

    private fun isRequiredPackage(packageName: CharSequence): Boolean {
        return when (packageName) {
            Data_TaxiApp.PACKAGE_GETT, Data_TaxiApp.PACKAGE_UBER, Data_TaxiApp.PACKAGE_YANDEX, Data_TaxiApp.PACKAGE_OPTEUM, Data_TaxiApp.PACKAGE_TAXSEE, Data_TaxiApp.PACKAGE_LYFT, Data_TaxiApp.PACKAGE_CITYMOBIL -> true
            else -> false
        }
    }

    private fun getNodesAndString(rootInActiveWindow: AccessibilityNodeInfo): Pair<NodesList, String> {
        val nodesInfoBuilder = StringBuilder()
        val nodes = NodesUtil.getAll(rootInActiveWindow, nodesInfoBuilder)
        return Pair(nodes, nodesInfoBuilder.toString())
    }

    private fun createManagerById(id: String): TaxiAppManager {
        return when (id) {
            Data_TaxiApp.ID_GETT -> ManagerGett({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger)
            Data_TaxiApp.ID_UBER -> ManagerUber({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger)
            Data_TaxiApp.ID_OPTEUM -> ManagerOpteum({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger)
            Data_TaxiApp.ID_TAXSEE -> ManagerTaxSee({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger, globalAction)
            Data_TaxiApp.ID_LYFT -> ManagerLyft({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger)
            Data_TaxiApp.ID_CITYMOBIL -> ManagerCitymobil({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger)
            Data_TaxiApp.ID_YANDEX -> ManagerYandex({ doNextTask() }, { appId, isOn -> onOrderEvent(appId, isOn) }, walkLogger, dispatchGesture)
            else -> {
                throw Exception("TaxiAppId '$id' doesn't exist")
            }
        }
    }

    fun onAccessibilityUnbound() {
        val say = String.format(strings[R.string.msg_err_service_unbound], strings[R.string.app_name])
        Toast.makeText(ctx, say, Toast.LENGTH_LONG).show()
        tts.sayFlush(say)
    }
}