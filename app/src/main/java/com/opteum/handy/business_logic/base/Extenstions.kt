package com.opteum.handy.business_logic.base

import android.view.accessibility.AccessibilityEvent

fun AccessibilityEvent.isClass(f: (strs: Array<String>) -> String?): Boolean = f(arrayOf(this.className.toString())) != null