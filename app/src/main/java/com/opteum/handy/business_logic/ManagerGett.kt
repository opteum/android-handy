package com.opteum.handy.business_logic

import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.R
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.ui.base.ifFalse

class ManagerGett(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_GETT, Data_TaxiApp.PACKAGE_GETT, Data_TaxiApp.NAME_GETT, failsLog, 2000L) {

    companion object {
        private const val APP_LOADING_MAX_TIME_MS = 20000L
        private const val DONE_DELAY_FOR_SWITCHER_CLICK = 250L
    }

    private val resIdOfferFragment = JniHandy::gett0resIdOfferFragment
    private val resIdLoadingLayout = JniHandy::gett0resIdLoadingLayout
    private val resIdAuthField = JniHandy::gett0resIdAuthField
    private val resIdAuthLoadingMsg = JniHandy::gett0resIdAuthLoadingMsg
    private val resIdSwitcher = JniHandy::gett0resIdSwitcher
    private val resIdRideButton = JniHandy::gett0resIdRideButton
    private val resIdMsgDialog = JniHandy::gett0resIdMsgDialog
    private val resIdBookingLayout = JniHandy::gett0resIdBookingLayout
    private val resIdBookingOrderInfoLayout = JniHandy::gett0resIdBookingOrderInfoLayout
    private val resIdDriverToolbar = JniHandy::gett0resIdDriverToolbar
    private val resIdToolbar = JniHandy::gett0resIdToolbar
    private val resIdHistoryOrderTitle = JniHandy::gett0resIdHistoryOrderTitle
    private val resIdAssignedOrder = JniHandy::gett0resIdAssignedOrder
    private val resIdInfoDialog = JniHandy::gett0resIdInfoDialog
    private val resIdQuestionDialog = JniHandy::gett0resIdQuestionDialog
    private val resIdStats = JniHandy::gett0resIdStats

    private var isLoadWarn = false

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdRideButton] != null && nodes[resIdSwitcher] == null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdSwitcher] != null && nodes[resIdRideButton] == null
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleFreeOrBusyTask(event, nodes)
        }
    }

    private fun handleFreeOrBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        val switcher = nodes[resIdSwitcher]

        // stage 1
        if (switcher != null) {
            val isBusy = !switcher.isChecked
            if (!isBusy) {
                switcher.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            }

            taskDone(callbackDelayMs = DONE_DELAY_FOR_SWITCHER_CLICK)
        }
        // optional // loading? lets wait a bit longer
        else if (nodes[resIdLoadingLayout] != null && nodes[resIdAuthLoadingMsg] != null) {
            isLoadWarn.ifFalse { say(R.string.msg_waiting_launching_done_or_start_in_advance, true); isLoadWarn = true }
            startSequenceWentWrongCountdown(APP_LOADING_MAX_TIME_MS)
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_RIDE_VIEW_DETECTED, true, getRideViewDetectedMsg())
        }
        // optional // auth form. nothing to do
        else if (nodes[resIdLoadingLayout] != null || nodes[resIdAuthField] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_AUTH_VIEW_DETECTED, true, getAuthViewDetectedMsg())
        }
        // optional // offer form. nothing to do
        else if (nodes[resIdOfferFragment] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_OFFR_VIEW_DETECTED, true, getOfferViewDetectedMsg())
        }
        // optional // assigned order info form. nothing to do
        else if (nodes[resIdAssignedOrder] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_ASGN_VIEW_DETECTED, true, getAssignedViewDetectedMsg())
        }
        // optional // question or info form. nothing to do
        else if (nodes[resIdMsgDialog] != null || nodes[resIdInfoDialog] != null || nodes[resIdQuestionDialog] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // smart // booking form. go back
        else if (nodes[resIdBookingLayout] != null) {
            tryClickBackOnDriverOrSimpleToolbar(nodes).ifFalse { taskFailNotifyAndDone() }
        }
        // smart // booking order info form. go back
        else if (nodes[resIdBookingOrderInfoLayout] != null) {
            tryClickBackOnDriverOrSimpleToolbar(nodes).ifFalse { taskFailNotifyAndDone() }
        }
        // smart // stats form. go back
        else if (nodes[resIdStats] != null) {
            tryClickBackOnDriverOrSimpleToolbar(nodes).ifFalse { taskFailNotifyAndDone() }
        }
        // smart // history form. go back
        else if (nodes[resIdHistoryOrderTitle] != null) {
            tryClickBackOnDriverOrSimpleToolbar(nodes).ifFalse { taskFailNotifyAndDone() }
        }
    }

    private fun tryClickBackOnDriverOrSimpleToolbar(nodes: NodesList): Boolean {
        (nodes[resIdDriverToolbar] ?: nodes[resIdToolbar])?.let {
            for (i in 0 until it.childCount) {
                val btnBack = it.getChild(i)
                if (btnBack.isClickable) {
                    btnBack.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                    return true
                }
            }
        }

        return false
    }

    override fun onResetSequenceCase() {
        isLoadWarn = false
    }
}
