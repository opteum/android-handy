package com.opteum.handy.business_logic

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.graphics.Rect
import android.os.Build
import android.os.Handler
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.R
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.ui.base.ifFalse
import com.opteum.handy.utils.GestureUtil
import com.opteum.handy.utils.GestureUtil.GESTURE_MS

@Suppress("DEPRECATION")
class ManagerYandex(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder, private val dispatchGesture: (gesture: GestureDescription, callback: AccessibilityService.GestureResultCallback?, handler: Handler?) -> Boolean) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_YANDEX, Data_TaxiApp.PACKAGE_YANDEX, Data_TaxiApp.NAME_YANDEX, failsLog) {

    companion object {
        private const val APP_CLOSING_MAX_TIME_MS = 10000L
        private const val APP_LOADING_MAX_TIME_MS = 25000L
        private const val LOWER_24API_NO_STAGE_ACTIONS_TIMEOUT_MS = 4000L
        private const val DONE_DELAY_FOR_SWITCHER_CLICK = 750L
    }

    private val resIdRideButtonOk = JniHandy::yandex0resIdRideButtonOk
    private val resIdRatingFeedback = JniHandy::yandex0resIdRatingFeedback
    private val resIdGridButtonText = JniHandy::yandex0resIdGridButtonText
    private val resIdBtnHome = JniHandy::yandex0resIdBtnHome
    private val resIdBtnYes = JniHandy::yandex0resIdBtnYes
    private val resIdProgressIndicator = JniHandy::yandex0resIdProgressIndicator
    private val resIdProgressMessage = JniHandy::yandex0resIdProgressMessage
    private val resIdAuthContent = JniHandy::yandex0resIdAuthContent
    private val resIdAuthPhone = JniHandy::yandex0resIdAuthPhone
    private val resIdAuthParkBtn = JniHandy::yandex0resIdAuthParkBtn
    private val resIdMessage = JniHandy::yandex0resIdMessage
    private val resIdSwitcher = JniHandy::yandex0resIdSwitcher
    private val resIdRequest = JniHandy::yandex0resIdRequest
    private val resIdRequestView = JniHandy::yandex0resIdRequestView
    private val resIdChooseLanguage = JniHandy::yandex0resIdChooseLanguage
    private val resIdDriverBtn = JniHandy::yandex0resIdDriverBtn

    private var orderDetection_isLastStateChangedComment = false

    private var stageDone = 0
    private var lastStageCompletionTask: (() -> Unit)? = null

    private var isLoadWarn = false

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdRideButtonOk] != null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        if (event.eventType != AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) return false

        val isLastCommentView = orderDetection_isLastStateChangedComment
        val isCurrCommentView = nodes[resIdRatingFeedback] != null
        orderDetection_isLastStateChangedComment = isCurrCommentView

        return nodes[resIdRideButtonOk] == null && isLastCommentView && !isCurrCommentView
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleBusyTask(event, nodes)
        }
    }

    private fun handleBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        // optional // offer check not implemented for yandex. it doesn't seem to be required.

        // optional // loading? lets wait a bit longer
        if (nodes[resIdAuthContent] != null && nodes[resIdAuthPhone] == null && nodes[resIdAuthParkBtn] == null) {
            isLoadWarn.ifFalse { say(R.string.msg_waiting_launching_done_or_start_in_advance, true); isLoadWarn = true }
            startSequenceWentWrongCountdown(APP_LOADING_MAX_TIME_MS)
        }
        // optional // some auth view. phone/licence/park selection
        else if (nodes[resIdAuthContent] != null && (nodes[resIdAuthPhone] != null || nodes[resIdChooseLanguage] != null)) {
            startSequenceWentWrongCountdown(0L, true, getSkipViewDetectedMsg(), LOG_INFO_SKIP_VIEW_DETECTED, true)
        }
        // optional // parks view or request info view
        else if (nodes[resIdAuthParkBtn] != null || nodes[resIdRequest] != null || nodes[resIdRequestView] != null) {
            startSequenceWentWrongCountdown(0L, true, getSkipViewDetectedMsg(), LOG_INFO_SKIP_VIEW_DETECTED, true)
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            startSequenceWentWrongCountdown(0L, true, getRideViewDetectedMsg(), LOG_INFO_RIDE_VIEW_DETECTED, true)
        }
        // optional // message view
        else if (nodes[resIdMessage]?.isClickable == true) {
            startSequenceWentWrongCountdown(0L, true, getSkipViewDetectedMsg(), LOG_INFO_SKIP_VIEW_DETECTED, true)
            nodes[resIdMessage]?.let { if (it.isClickable) it.performAction(AccessibilityNodeInfo.ACTION_CLICK) }
        }
        // API fork
        else {
            // 24+
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                api24moreSequence(nodes)
            }
            // 24-
            else {
                api24lessSequence(nodes)
            }
        }
    }

    private fun api24moreSequence(nodes: NodesList) {
        val switcher = nodes[resIdSwitcher]

        if (switcher != null) {
            val bounds = Rect()
            switcher.getBoundsInScreen(bounds)
            GestureUtil.toLeftOrToDown(bounds, dispatchGesture)
            taskDone(callbackDelayMs = GESTURE_MS + DONE_DELAY_FOR_SWITCHER_CLICK)
        } else {
            taskFailNotifyAndDone()
        }
    }

    private var isNoStagesActionDone = false

    private fun api24lessSequence(nodes: NodesList) {
        // click btn if exist
        if (stageDone == 0 && nodes[resIdGridButtonText] != null) {
            startSequenceWentWrongCountdown()
            nodes[resIdGridButtonText]?.parent?.let { if (it.isClickable) it.performAction(AccessibilityNodeInfo.ACTION_CLICK) }
            stageDone = 1
        }
        // confirm dialog
        else if (stageDone == 1 && nodes[resIdBtnYes] != null) {
            startSequenceWentWrongCountdown()
            nodes[resIdBtnYes]?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            stageDone = 2
        }
        // wait progress
        else if (stageDone == 2 && nodes[resIdProgressIndicator] != null && nodes[resIdProgressMessage] != null) {
            startSequenceWentWrongCountdown(APP_CLOSING_MAX_TIME_MS)
            lastStageCompletionTask = { taskDone() }
        }
        // if no exit, start sequence to exit
        else if (nodes[resIdBtnHome] != null && !isNoStagesActionDone) {
            startSequenceWentWrongCountdown(LOWER_24API_NO_STAGE_ACTIONS_TIMEOUT_MS)
            nodes[resIdDriverBtn]?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            nodes[resIdBtnHome]?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            nodes[resIdBtnHome]?.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            isNoStagesActionDone = true
        }
    }

    override fun onLeaveWhileTask() {
        lastStageCompletionTask?.invoke()
    }

    override fun onResetSequenceCase() {
        stageDone = 0
        lastStageCompletionTask = null
        isNoStagesActionDone = false
        orderDetection_isLastStateChangedComment = false
        isLoadWarn = false
    }
}