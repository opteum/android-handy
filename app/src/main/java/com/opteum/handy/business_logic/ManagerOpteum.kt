package com.opteum.handy.business_logic

import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp

class ManagerOpteum(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_OPTEUM, Data_TaxiApp.PACKAGE_OPTEUM, Data_TaxiApp.NAME_OPTEUM, failsLog) {

    private val resIdLoginField = JniHandy::opteum0resIdLoginField
    private val resIdStartViewBtn = JniHandy::opteum0resIdStartViewBtn
    private val resIdSwitcher = JniHandy::opteum0resIdSwitcher
    private val resIdBtnYes = JniHandy::opteum0resIdBtnYes
    private val resIdBtnNo = JniHandy::opteum0resIdBtnNo
    private val resIdOffersCancelBtn = JniHandy::opteum0resIdOffersCancelBtn
    private val resIdLblBusy = JniHandy::opteum0resIdLblBusy
    private val resIdDriverAvatar = JniHandy::opteum0resIdDriverAvatar
    private val resIdMessages = JniHandy::opteum0resIdMessages
    private val resIdWorking = JniHandy::opteum0resIdWorking
    private val resIdStatusBtn = JniHandy::opteum0resIdStatusBtn

    private var stageDone = 0

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdStatusBtn] != null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdWorking] != null
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleBusyTask(event, nodes)
        }
    }

    private fun handleBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        val switcher = nodes[resIdSwitcher]

        // stage 1
        if (stageDone == 0 && switcher != null) {
            startSequenceWentWrongCountdown()

            val isBusy = nodes[resIdLblBusy]?.isEnabled ?: false

            if (!isBusy) {
                switcher.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                stageDone = 1
            } else {
                taskDone()
            }
        }
        // stage 2
        else if (stageDone == 1 && nodes[resIdBtnYes] != null) {
            startSequenceWentWrongCountdown()

            nodes[resIdBtnYes]?.let {
                it.performAction(AccessibilityNodeInfo.ACTION_CLICK)
                taskDone()
            }
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_RIDE_VIEW_DETECTED, true, getRideViewDetectedMsg())
        }
        // optional // offer form. nothing to do
        else if (nodes[resIdOffersCancelBtn] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_OFFR_VIEW_DETECTED, true, getOfferViewDetectedMsg())
        }
        // optional // auth form. nothing to do
        else if (nodes[resIdLoginField] != null || nodes[resIdStartViewBtn] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_AUTH_VIEW_DETECTED, true, getAuthViewDetectedMsg())
        }
        // optional // profile form. nothing to do
        else if (nodes[resIdDriverAvatar] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // optional // messages form. nothing to do
        else if (nodes[resIdMessages] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // optional // opened alert dialog. close it.
        else if (stageDone == 0 && nodes[resIdBtnNo] != null) {
            nodes[resIdBtnNo]?.performAction(AccessibilityNodeInfo.ACTION_CLICK) ?: taskFailNotifyAndDone()
        }
    }

    override fun onResetSequenceCase() {
        stageDone = 0
    }
}