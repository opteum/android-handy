package com.opteum.handy.business_logic

import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.R
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.ui.base.ifFalse

class ManagerCitymobil(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_CITYMOBIL, Data_TaxiApp.PACKAGE_CITYMOBIL, Data_TaxiApp.NAME_CITYMOBIL, failsLog) {

    companion object {
        private const val APP_LOADING_MAX_TIME_MS = 20000L
    }

    private val resIdBtnSetBusy = JniHandy::citymobil0resIdBtnSetBusy
    private val resIdBtnPositive = JniHandy::citymobil0resIdBtnPositive
    private val resIdDialogMessage = JniHandy::citymobil0resIdDialogMessage
    private val resIdBtnOfferAccept = JniHandy::citymobil0resIdBtnOfferAccept
    private val resIdBtnArrived = JniHandy::citymobil0resIdBtnArrived
    private val resIdBtnGo = JniHandy::citymobil0resIdBtnGo
    private val resIdBtnDone = JniHandy::citymobil0resIdBtnDone
    private val resIdSplash = JniHandy::citymobil0resIdSplash
    private val resIdRegister = JniHandy::citymobil0resIdRegister

    private var isPreClosedView = false

    private var isLoadWarn = false

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[resIdBtnArrived] != null || nodes[resIdBtnGo] != null || nodes[resIdBtnDone] != null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        if (nodes[resIdDialogMessage] != null && nodes[resIdBtnPositive] != null) {
            isPreClosedView = true
            return false
        } else if (isPreClosedView && (nodes[resIdBtnArrived] == null && nodes[resIdBtnGo] == null && nodes[resIdBtnDone] == null)) {
            isPreClosedView = false
            return true
        } else {
            isPreClosedView = false
            return false
        }
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleBusyTask(event, nodes)
        }
    }

    private fun handleBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        val btnSetBusy = nodes[resIdBtnSetBusy]

        if (btnSetBusy != null) {
            btnSetBusy.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            taskDone()
        }
        // optional // loading view. wait
        else if (nodes[resIdSplash] != null) {
            isLoadWarn.ifFalse { say(R.string.msg_waiting_launching_done_or_start_in_advance, true); isLoadWarn = true }
            startSequenceWentWrongCountdown(APP_LOADING_MAX_TIME_MS)
        }
        // optional // register view. skip
        else if (nodes[resIdRegister] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_SKIP_VIEW_DETECTED, true, getSkipViewDetectedMsg())
        }
        // optional // offer form. nothing to do
        else if (nodes[resIdBtnOfferAccept] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_OFFR_VIEW_DETECTED, true, getOfferViewDetectedMsg())
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_RIDE_VIEW_DETECTED, true, getRideViewDetectedMsg())
        }
    }

    override fun onResetSequenceCase() {
        isLoadWarn = false
    }
}