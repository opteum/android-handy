package com.opteum.handy.business_logic

import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.business_logic.base.TaxiAppManager
import com.opteum.handy.jni.JniHandy
import com.opteum.handy.jni.isIt
import com.opteum.handy.metrics.ITaxiEventsRecorder
import com.opteum.handy.models.Data_TaxiApp

class ManagerLyft(taskDoneCallback: (() -> Unit), orderCallback: ((appId: String, isOn: Boolean) -> Unit), failsLog: ITaxiEventsRecorder) :
        TaxiAppManager(taskDoneCallback, orderCallback, Data_TaxiApp.ID_LYFT, Data_TaxiApp.PACKAGE_LYFT, Data_TaxiApp.NAME_LYFT, failsLog) {

    private val btnToggle = JniHandy::lyft0resIdBtnToggle
    private val containerDriverScreen = JniHandy::lyft0resIdDriverRideContainer
    private val btnToggleValueOnline = JniHandy::lyft0valueBtnToggleOnline
    private val btnToggleValueLastRide = JniHandy::lyft0valueBtnToggleLastRide
    private val resIdAcceptOrder = JniHandy::lyft0resIdAcceptOrder

    override fun isOrderOpenedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[containerDriverScreen] != null
    }

    override fun isOrderClosedView(event: AccessibilityEvent, nodes: NodesList): Boolean {
        return nodes[containerDriverScreen] == null
    }

    override fun onNewViewContentIfAnyTask(event: AccessibilityEvent, nodes: NodesList) {
        if (task == Task.BUSY) {
            handleBusyTask(event, nodes)
        }
    }

    private fun handleBusyTask(event: AccessibilityEvent, nodes: NodesList) {
        val btnToggle = nodes[btnToggle]

        if (btnToggle != null) {
            val btnToggleText = btnToggle.text.toString().toLowerCase()

            if (btnToggleValueOnline.isIt(btnToggleText) || btnToggleValueLastRide.isIt(btnToggleText)) {
                btnToggle.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            }
            taskDone()
        }
        // optional // offer form. nothing to do
        else if (nodes[resIdAcceptOrder] != null) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_OFFR_VIEW_DETECTED, true, getOfferViewDetectedMsg())
        }
        // optional // ride form. nothing to do
        else if (isOrderOpenedView(event, nodes)) {
            taskUndefinedResultNotifyAndDone(LOG_INFO_RIDE_VIEW_DETECTED, true, getRideViewDetectedMsg())
        }
    }
}