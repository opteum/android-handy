package com.opteum.handy.business_logic.utils

import android.view.accessibility.AccessibilityNodeInfo
import com.opteum.handy.business_logic.base.NodesList
import com.opteum.handy.ui.base.toBin
import java.lang.StringBuilder

object NodesUtil {
    fun getAll(node: AccessibilityNodeInfo, outStringResult: StringBuilder? = null): NodesList {
        ArrayList<AccessibilityNodeInfo>()
        val map = HashMap<String, AccessibilityNodeInfo>()
        val nodesIds = ArrayList<String>()

        doProcessAllNodes(node, 0, { map.put(it.viewIdResourceName, it); nodesIds.add(it.viewIdResourceName ?: "null") }, outStringResult)

        return NodesList(map, nodesIds)
    }

    private fun doProcessAllNodes(node: AccessibilityNodeInfo, deep: Int, action: (node: AccessibilityNodeInfo) -> Unit, outStringResult: StringBuilder? = null) {
        outStringResult?.let {
            it.append((0..deep).joinToString("=", transform = { "" }))
            it.append(" ")
            it.append("${node.isClickable.toBin()}${node.isCheckable.toBin()}${node.isChecked.toBin()}")
            it.append(" ")
            it.append(node.viewIdResourceName)
            it.append(" ")
            it.append(node.text)
            it.appendln()
        }

        action(node)

        for (i in 0 until node.childCount) {
            val child = node.getChild(i) ?: continue
            doProcessAllNodes(child, deep + 1, action, outStringResult)
        }
    }
}