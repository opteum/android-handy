package com.opteum.handy.data

import android.content.Context
import android.preference.PreferenceManager

class AppPrefs(private val ctx: Context) {
    private val pref = PreferenceManager.getDefaultSharedPreferences(ctx)

    fun isContains(res: Int): Boolean {
        return pref.contains(ctx.getString(res))
    }

    fun put(res: Int, value: String) {
        pref.edit().putString(ctx.getString(res), value).apply()
    }

    fun get(res: Int): String? {
        return pref.getString(ctx.getString(res), null)
    }

    fun getBool(res: Int): Boolean {
        return pref.getBoolean(ctx.getString(res), false)
    }

    fun putBool(res: Int, value: Boolean) {
        pref.edit().putBoolean(ctx.getString(res), value).apply()
    }

    fun getLong(res: Int): Long {
        return pref.getLong(ctx.getString(res), 0L)
    }

    fun putLong(res: Int, value: Long) {
        pref.edit().putLong(ctx.getString(res), value).apply()
    }

    fun getInt(res: Int): Int {
        return pref.getInt(ctx.getString(res), 0)
    }

    fun putInt(res: Int, value: Int) {
        pref.edit().putInt(ctx.getString(res), value).apply()
    }

    fun remove(res: Int) {
        pref.edit().remove(ctx.getString(res)).apply()
    }

    fun removeAll() {
        pref.edit().clear().apply()
    }
}