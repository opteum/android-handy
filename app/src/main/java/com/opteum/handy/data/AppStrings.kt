package com.opteum.handy.data

import android.content.Context

class AppStrings(private val ctx: Context) {
    operator fun get(resId: Int): String {
        return ctx.getString(resId)
    }
}