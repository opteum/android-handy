package com.opteum.handy

import android.app.Application
import android.content.Context
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton
import com.google.gson.Gson
import com.opteum.handy.data.AppPrefs
import com.opteum.handy.data.AppStrings
import com.opteum.handy.features.Tts
import com.opteum.handy.metrics.WalkthroughLogger
import com.opteum.handy.models.cache.TaxiAppsCache
import com.opteum.handy.network.Api
import com.opteum.handy.network.ApiBuilder
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.network.AuthToken
import com.opteum.handy.ui.base.FragmentChanger
import com.opteum.handy.utils.AppUtil
import com.yandex.metrica.YandexMetrica

class App : Application() {
    companion object {
        var kodein = Kodein { }
    }

    override fun onCreate() {
        super.onCreate()

        initDI()
        initYandexMetrica()
    }

    private fun initDI() {
        kodein = Kodein {
            bind<Gson>() with singleton { Gson() }
            bind<Context>() with singleton { this@App }
            bind<AppUtil>() with singleton { AppUtil(this@App) }
            bind<AppStrings>() with singleton { AppStrings(this@App) }
            bind<AppPrefs>() with singleton { AppPrefs(this@App) }
            bind<Tts>() with singleton { Tts(this@App) }
            bind<AuthToken>() with singleton { AuthToken() }
            bind<Api>() with singleton { ApiBuilder().build() }
            bind<AppApiCallExecutor>() with singleton { AppApiCallExecutor() }
            bind<FragmentChanger>() with singleton { FragmentChanger }
            bind<TaxiAppsCache>() with singleton { TaxiAppsCache() }
            bind<WalkthroughLogger>() with singleton { WalkthroughLogger() }
        }
    }

    private fun initYandexMetrica() {
        if (BuildConfig.DEBUG) {
            YandexMetrica.setReportCrashesEnabled(false)
            YandexMetrica.setReportNativeCrashesEnabled(false)
            YandexMetrica.setTrackLocationEnabled(false)
        }

        YandexMetrica.activate(applicationContext, getString(R.string.app_yandex_api))
        YandexMetrica.enableActivityAutoTracking(this)
    }
}