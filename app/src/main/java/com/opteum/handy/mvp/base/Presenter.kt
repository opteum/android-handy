package com.opteum.handy.mvp.base

abstract class Presenter<T> {
    private var view: T? = null
    private var isInited = false
    private val viewDelayedActions = ArrayList<T.() -> Unit>()

    open fun onStarted() {}
    abstract fun onFirstStart()

    fun onStart(view: T) {
        this.view = view

        if (!isInited) {
            onFirstStart()
            isInited = true
        }

        onStarted()

        viewDelayedActions.forEach { view.it() }
        viewDelayedActions.clear()
    }

    fun onStop() {
        this.view = null
    }

    fun view(viewAction: T.() -> Unit) {
        if (view == null) {
            viewDelayedActions.add(viewAction)
        } else {
            view?.viewAction()
        }
    }
}