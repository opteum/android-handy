package com.opteum.handy.mvp.base

import com.opteum.handy.ui.base.InteractorResult

object DefaultMvpInteractorResultHandler {
    fun <T> doDefaultHandle(interactorResult: InteractorResult<T>, view: IView, successAction: (data: T) -> Unit) {
        if (interactorResult.isSuccess) {
            interactorResult.data?.let(successAction)
        } else {
            interactorResult.errorMessage?.let { view.showMessage(it) }
        }
    }

    fun doDefaultHandle(interactorResult: InteractorResult<Unit>, view: IView, successAction: () -> Unit) {
        if (interactorResult.isSuccess) {
            successAction.invoke()
        } else {
            interactorResult.errorMessage?.let { view.showMessage(it) }
        }
    }
}