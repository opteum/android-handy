package com.opteum.handy.mvp

import android.content.Context
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.network.responses.Response_PaymentData
import com.opteum.handy.ui.base.InteractorResult

class Presenter_PaymentInfo : Presenter<IView_PaymentInfo>() {
    private val interactorPayment = Interactor_Payment()
    private val interactorAuth = Interactor_Auth()
    private lateinit var paymentDataResult: Response_PaymentData
    private val appCtx: Context = App.kodein.instance()

    override fun onFirstStart() {
        view { showProgress() }
        interactorPayment.getPaymentData(this::onDataResult)
    }

    private fun onDataResult(result: InteractorResult<Response_PaymentData>) {
        result.data?.let { paymentDataResult = it }

        view {
            hideProgress()
            if (result.isSuccess) {
                showData(paymentDataResult.title, paymentDataResult.description, paymentDataResult.tariffValue, paymentDataResult.expiredDateMs)
            } else {
                Toast.makeText(appCtx, R.string.text_response_error, Toast.LENGTH_SHORT).show()
                Toast.makeText(appCtx, R.string.text_check_connection, Toast.LENGTH_LONG).show()
                close()
            }
        }
    }

    fun onPayClick() {
        if (interactorAuth.isTokenExist()) {
            view { goToPayView(paymentDataResult.tariffValue.toString()) }
        } else {
            view { showLoginOnRegisterDialog() }
        }
    }

    fun onRejectClick() {
        view { goToRejectView() }
    }
}