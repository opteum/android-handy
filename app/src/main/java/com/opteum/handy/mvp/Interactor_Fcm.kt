package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.iid.FirebaseInstanceId
import com.opteum.handy.App
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.ui.base.InteractorResult
import com.opteum.handy.utils.AppUtil

class Interactor_Fcm {
    private val ctx: Context = App.kodein.instance()
    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()

    fun sendToken(callback: (result: InteractorResult<Unit>) -> Unit) {
        val availableResultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(ctx)

        val token = if (availableResultCode == 0) {
            FirebaseInstanceId.getInstance().token ?: ""
        } else {
            ""
        }

        apiExe.executeApiCall(api.sendToken(token), callback)
    }
}