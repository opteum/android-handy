package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppPrefs
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.models.cache.TaxiAppsCache
import com.opteum.handy.utils.AppsUtil
import java.lang.IllegalArgumentException

class Interactor_Controls {
    private val ctx: Context = App.kodein.instance()
    private val pref: AppPrefs = App.kodein.instance()

    private var cacheTaxiApps: TaxiAppsCache = App.kodein.instance()

    fun isEnabled(): Boolean {
        return pref.getBool(R.string.pref_is_enabled)
    }

    fun isTtsEnabled(): Boolean {
        return if (pref.isContains(R.string.pref_is_tts_enabled)) {
            pref.getBool(R.string.pref_is_tts_enabled)
        } else {
            pref.putBool(R.string.pref_is_tts_enabled, true)
            true
        }
    }

    fun getTaxiAppsCurrentStateList(): ArrayList<Data_TaxiApp> {
        cacheTaxiApps.get()?.let { return it }

        val list = ArrayList<Data_TaxiApp>()

        val apps = AppsUtil.getAllInstalledApps(ctx)
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_UBER, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_UBER, Data_TaxiApp.NAME_UBER, R.drawable.ic_active_uber, R.drawable.ic_sleep_uber, pref.getBool(R.string.pref_app_enabled_uber)))
        }
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_GETT, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_GETT, Data_TaxiApp.NAME_GETT, R.drawable.ic_active_gett, R.drawable.ic_sleep_gett, pref.getBool(R.string.pref_app_enabled_gett)))
        }
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_OPTEUM, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_OPTEUM, Data_TaxiApp.NAME_OPTEUM, R.drawable.ic_active_opteum, R.drawable.ic_sleep_opteum, pref.getBool(R.string.pref_app_enabled_opteum)))
        }
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_TAXSEE, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_TAXSEE, Data_TaxiApp.NAME_TAXSEE, R.drawable.ic_active_taxsee, R.drawable.ic_sleep_taxsee, pref.getBool(R.string.pref_app_enabled_taxsee)))
        }
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_LYFT, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_LYFT, Data_TaxiApp.NAME_LYFT, R.drawable.ic_lyft_active, R.drawable.ic_lyft_sleep, pref.getBool(R.string.pref_app_enabled_lyft)))
        }
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_CITYMOBIL, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_CITYMOBIL, Data_TaxiApp.NAME_CITYMOBIL, R.drawable.ic_citymobil_active, R.drawable.ic_citymobil_sleep, pref.getBool(R.string.pref_app_enabled_citymobil)))
        }
        if (AppsUtil.isInstalled(Data_TaxiApp.PACKAGE_YANDEX, apps)) {
            list.add(Data_TaxiApp(Data_TaxiApp.ID_YANDEX, Data_TaxiApp.NAME_YANDEX, R.drawable.ic_active_yandextaxi, R.drawable.ic_sleep_yandextaxi, pref.getBool(R.string.pref_app_enabled_yandex)))
        }

        return list.apply { cacheTaxiApps.set(this) }
    }

    fun setTaxiAppEnabled(id: String, setEnabled: Boolean) {
        var prefRes = 0
        when (id) {
            Data_TaxiApp.ID_UBER -> prefRes = R.string.pref_app_enabled_uber
            Data_TaxiApp.ID_GETT -> prefRes = R.string.pref_app_enabled_gett
            Data_TaxiApp.ID_YANDEX -> prefRes = R.string.pref_app_enabled_yandex
            Data_TaxiApp.ID_TAXSEE -> prefRes = R.string.pref_app_enabled_taxsee
            Data_TaxiApp.ID_LYFT -> prefRes = R.string.pref_app_enabled_lyft
            Data_TaxiApp.ID_CITYMOBIL -> prefRes = R.string.pref_app_enabled_citymobil
            Data_TaxiApp.ID_OPTEUM -> prefRes = R.string.pref_app_enabled_opteum
        }

        if (prefRes == 0) {
            throw IllegalArgumentException()
        }

        pref.putBool(prefRes, setEnabled)

        // update cacheTaxiApps
        cacheTaxiApps.get()?.firstOrNull { it.id == id }?.let { it.isEnabled = setEnabled }
    }

    fun setEnabled(enabled: Boolean) {
        pref.putBool(R.string.pref_is_enabled, enabled)
    }

    fun setTtsEnabled(enabled: Boolean) {
        pref.putBool(R.string.pref_is_tts_enabled, enabled)
    }
}