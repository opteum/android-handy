package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.BuildConfig
import com.opteum.handy.R
import com.opteum.handy.data.AppPrefs
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.ui.base.InteractorResult
import com.opteum.handy.utils.AppUtil
import org.json.JSONException
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class Interactor_Feedback {
    companion object {
        private val JSON_KEY_RATING = "RatingFromUser"
        private val JSON_KEY_FEEDBACK = "ReviewFromUser"
        private val FEEDBACK_SECRET = "sdaf4e2r45t45re4a5g4deVF5sdf4s5af"
    }

    private val appUtil: AppUtil = App.kodein.instance()
    private val ctx: Context = App.kodein.instance()
    private val pref: AppPrefs = App.kodein.instance()
    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()
    private val daysToRequest = ctx.resources.getInteger(R.integer.feedback_request_days)

    fun isFeedbackRequestTime(): Boolean {
        val startTimeMs = pref.getLong(R.string.pref_feedback_request_time_start_ms)
        if (startTimeMs == 0L) {
            pref.putLong(R.string.pref_feedback_request_time_start_ms, System.currentTimeMillis())
            return false
        }

        if (startTimeMs == -1L) return false

        val msDiff = System.currentTimeMillis() - startTimeMs
        val daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff)

        if (daysDiff >= daysToRequest) {
            pref.putLong(R.string.pref_feedback_request_time_start_ms, -1L)
            return true
        } else {
            return false
        }
    }

    fun rating(rating: Float, callback: (result: InteractorResult<Unit>) -> Unit) {
        send("*HANDY R ${appUtil.getDeviceId()}* - ${BuildConfig.VERSION_NAME} - $rating", rating, callback)
    }

    fun message(text: String, callback: (result: InteractorResult<Unit>) -> Unit) {
        send("*HANDY F ${appUtil.getDeviceId()}* - ${BuildConfig.VERSION_NAME} - $text", 0f, callback)
    }

    fun fullFeedback(text: String, rating: Float, callback: (result: InteractorResult<Unit>) -> Unit) {
        send("*HANDY FR ${appUtil.getDeviceId()}* - ${BuildConfig.VERSION_NAME} - $text", rating, callback)
    }

    private fun send(text: String, rating: Float, callback: (result: InteractorResult<Unit>) -> Unit) {
        var dataToSend: String? = null
        try {
            JSONObject().apply {
                put(JSON_KEY_FEEDBACK, text)
                put(JSON_KEY_RATING, rating)
            }.let { dataToSend = it.toString() }
        } catch (ignored: JSONException) {
            callback.invoke(InteractorResult())
            return
        }

        dataToSend?.let { apiExe.executeApiCall(api.feedback(it, FEEDBACK_SECRET), callback) }
    }
}