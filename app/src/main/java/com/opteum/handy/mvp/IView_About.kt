package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView


interface IView_About : IView {
    fun rateOnStore()
    fun showData()
    fun showFeedbackDialog()
}