package com.opteum.handy.mvp

import android.content.Context
import android.content.Intent
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppStrings
import com.opteum.handy.metrics.YaHelper
import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.ui.base.ifFalse

class Presenter_Controls : Presenter<IView_Controls>() {
    companion object {
        val INTENT_ACTION_APPS_CONFIG_CHANGED = "INTENT_ACTION_APPS_CONFIG_CHANGED"
        val INTENT_ACTION_CURRENT_APPS_TO_BUSY = "INTENT_ACTION_CURRENT_APPS_TO_BUSY"
    }

    private val ctx: Context = App.kodein.instance()
    private val interactor = Interactor_Controls()
    private val strings: AppStrings = App.kodein.instance()

    override fun onFirstStart() {
        val isEnabled = interactor.isEnabled()
        view {
            setInitDataForView(isEnabled, interactor.getTaxiAppsCurrentStateList())
            invalidateStateView(isEnabled)
        }

        ctx.sendBroadcast(Intent(INTENT_ACTION_APPS_CONFIG_CHANGED))
    }

    private fun updateStateView() {
        view { invalidateStateView(interactor.isEnabled()) }
    }

    fun onChangeIsEnabled(setEnabled: Boolean) {
        interactor.setEnabled(setEnabled)
        updateStateView()

        if (setEnabled) {
            YaHelper.reportCheckedAppsOnEnabled(interactor.getTaxiAppsCurrentStateList())
            view { say(R.string.msg_open_main_forms_of_selected_apps) }
        } else {
            view { say(R.string.msg_no_more_apps_tracking) }
        }

        ctx.sendBroadcast(Intent(INTENT_ACTION_APPS_CONFIG_CHANGED))
    }

    fun onTaxiAppClick(item: Data_TaxiApp) {
        interactor.setTaxiAppEnabled(item.id, !item.isEnabled)
        updateStateView()

        item.isEnabled.ifFalse { view { say("\"${item.name}\" - ${ctx.getString(R.string.text_no_more_tracking).toLowerCase()}") } }

        ctx.sendBroadcast(Intent(INTENT_ACTION_APPS_CONFIG_CHANGED))
    }


    fun onTtsClick() {
        interactor.setTtsEnabled(!interactor.isTtsEnabled())
        view {
            invalidateTtsView()
            say("${strings[R.string.text_speech]} - ${strings[if (interactor.isTtsEnabled()) R.string.text_on_short else R.string.text_off_short]}")
        }
    }

    fun setCurrentAppsToBusy() {
        ctx.sendBroadcast(Intent(INTENT_ACTION_CURRENT_APPS_TO_BUSY))
    }
}