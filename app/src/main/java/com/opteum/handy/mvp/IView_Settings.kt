package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView


interface IView_Settings : IView {
    fun openPaymentView()
    fun openAboutView()
    fun openProfileView()
    fun invalidateAuthButtonsView(email: String?)
    fun showLoginOnRegisterDialog()
}