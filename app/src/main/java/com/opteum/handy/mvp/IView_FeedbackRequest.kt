package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView

interface IView_FeedbackRequest : IView {
    fun showData()
    fun close()
    fun rateOnStore()
}