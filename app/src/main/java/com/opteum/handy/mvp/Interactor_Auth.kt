package com.opteum.handy.mvp

import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppPrefs
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.network.AuthToken
import com.opteum.handy.network.responses.Response_Auth
import com.opteum.handy.ui.base.InteractorResult

class Interactor_Auth {
    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()
    private val prefs: AppPrefs = App.kodein.instance()
    private val authToken: AuthToken = App.kodein.instance()

    fun register(login: String, password: String, callback: (result: InteractorResult<Unit>) -> Unit) {
        apiExe.executeApiCall(api.register(login, password), callback)
    }

    fun auth(login: String, password: String, callback: (result: InteractorResult<Response_Auth>) -> Unit) {
        apiExe.executeApiCall(api.login(login, password), callback, { response ->
            // automatically save auth_token on success
            response?.data?.authToken?.let {
                prefs.put(R.string.pref_email, login)
                prefs.put(R.string.pref_auth_token, it)
                authToken.setCurrentToken(it)
            }
        }, { _, _ ->
            // automatically remove auth_token on any fail
            prefs.remove(R.string.pref_auth_token)
        })
    }

    fun logout(callback: (result: InteractorResult<Unit>) -> Unit) {
        apiExe.executeApiCall(api.logout(), callback, {
            authToken.setCurrentToken("")
            prefs.removeAll()
        })
    }

    fun isTokenExist(): Boolean {
        return prefs.get(R.string.pref_auth_token) != null
    }

    fun getEmail(): String? {
        return prefs.get(R.string.pref_email)
    }

    fun getToken(): String? {
        return prefs.get(R.string.pref_auth_token)
    }
}