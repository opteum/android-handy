package com.opteum.handy.mvp

import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.network.responses.Response_Profile
import com.opteum.handy.ui.base.InteractorResult

class Interactor_Profile {
    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()

    fun getData(callback: (result: InteractorResult<Response_Profile>) -> Unit) {
        apiExe.executeApiCall(api.getProfileData(), callback)
    }

    fun sendData(phone: String, name: String, callback: (result: InteractorResult<Unit>) -> Unit) {
        apiExe.executeApiCall(api.sendProfileData(phone, name), callback)
    }
}