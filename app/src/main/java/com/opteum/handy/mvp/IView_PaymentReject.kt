package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView


interface IView_PaymentReject : IView {
    fun close()
    fun showAnotherReasonDialog()
}