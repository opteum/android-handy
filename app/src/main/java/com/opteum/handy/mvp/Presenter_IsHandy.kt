package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.Presenter

class Presenter_IsHandy : Presenter<IView_IsHandy>() {
    override fun onFirstStart() {}

    fun onYesClick() {
        view { goToTellAbout() }
    }

    fun onNoClick() {
        view { goToFeedback() }
    }
}