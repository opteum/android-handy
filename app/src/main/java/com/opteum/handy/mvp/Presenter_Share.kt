package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.Presenter

class Presenter_Share : Presenter<IView_Share>() {
    override fun onFirstStart() {}

    fun onOkClick() {
        view { close() }
    }
}