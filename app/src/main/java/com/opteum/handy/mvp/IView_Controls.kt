package com.opteum.handy.mvp

import com.opteum.handy.models.Data_TaxiApp
import com.opteum.handy.mvp.base.IView
import java.util.*

interface IView_Controls : IView {
    fun setInitDataForView(isEnabled: Boolean, taxiApps: ArrayList<Data_TaxiApp>)
    fun invalidateStateView(isEnabled: Boolean, isNoAnimation: Boolean = false)
    fun invalidateTtsView()
}