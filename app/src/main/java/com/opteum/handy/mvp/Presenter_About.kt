package com.opteum.handy.mvp

import com.opteum.handy.R
import com.opteum.handy.mvp.base.DefaultMvpInteractorResultHandler
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.ui.base.InteractorResult

class Presenter_About : Presenter<IView_About>() {
    private val interactor = Interactor_Feedback()

    override fun onFirstStart() {
        view { showData() }
    }

    fun onSendRateClick(rating: Float) {
        view { showProgress() }
        interactor.rating(rating, { onRateResult(it, rating) })
    }

    fun onFeedbackClick() {
        view { showFeedbackDialog() }
    }

    fun sendFeedback(text: String) {
        if (text.isBlank()) return
        view { showProgress() }
        interactor.message(text, this::onFeedbackResult)
    }

    private fun onRateResult(result: InteractorResult<Unit>, rating: Float) {
        view {
            hideProgress()
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, {
                this.showMessage(R.string.text_operation_successful)
                if (rating >= 4f) {
                    this.rateOnStore()
                }
            })
        }
    }

    private fun onFeedbackResult(result: InteractorResult<Unit>) {
        view {
            hideProgress()
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, { this.showMessage(R.string.text_operation_successful) })
        }

    }
}