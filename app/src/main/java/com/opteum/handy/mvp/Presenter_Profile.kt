package com.opteum.handy.mvp

import com.opteum.handy.R
import com.opteum.handy.mvp.base.DefaultMvpInteractorResultHandler
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.network.responses.Response_Profile
import com.opteum.handy.ui.base.InteractorResult

class Presenter_Profile : Presenter<IView_Profile>() {
    private val interactor = Interactor_Profile()

    override fun onFirstStart() {
        view { showProgress() }
        interactor.getData(this::onGetDataResult)
    }

    fun onSaveClick(phone: String, name: String) {
        if (phone.isBlank() || name.isBlank()) {
            view { showMessage(R.string.err_field_is_empty) }
            return
        }

        view { showProgress() }
        interactor.sendData(phone, name, this::onSendDataResult)
    }

    private fun onGetDataResult(result: InteractorResult<Response_Profile>) {
        view {
            hideProgress()
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, { showData(it) })
        }
    }

    private fun onSendDataResult(result: InteractorResult<Unit>) {
        view {
            hideProgress()
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, { showMessage(R.string.text_operation_successful) })
        }
    }
}