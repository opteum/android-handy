package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.DefaultMvpInteractorResultHandler
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.ui.base.InteractorResult

class Presenter_FeedbackRequest : Presenter<IView_FeedbackRequest>() {
    private val interactor = Interactor_Feedback()

    override fun onFirstStart() {
        view { showData() }
    }

    fun onCloseClick() {
        view { close() }
    }

    fun sendFeedback(text: String, rating: Float) {
        if (text.isBlank()) return
        view { showProgress() }
        interactor.fullFeedback(text, rating, { onFeedbackResult(rating, it) })
    }

    private fun onFeedbackResult(rating: Float, result: InteractorResult<Unit>) {
        view { hideProgress() }
        view {
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, {
                if (rating >= 4f) {
                    this.rateOnStore()
                }

                close()
            })
        }
    }
}