package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView


interface IView_PaymentInfo : IView {
    fun goToRejectView()
    fun goToPayView(cost: String)
    fun showData(header: String, description: String, cost: Int, expiredDateMs: Long)
    fun showLoginOnRegisterDialog()
    fun close()
}