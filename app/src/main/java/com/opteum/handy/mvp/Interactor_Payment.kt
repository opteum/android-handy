package com.opteum.handy.mvp

import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.network.responses.Response_PaymentData
import com.opteum.handy.ui.base.InteractorResult

class Interactor_Payment {
    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()

    fun sendRejectReason(rejectReason: String, callback: (result: InteractorResult<Unit>) -> Unit) {
        apiExe.executeApiCall(api.sendRejectReason(rejectReason), callback)
    }

    fun getPaymentData(callback: (result: InteractorResult<Response_PaymentData>) -> Unit) {
        apiExe.executeApiCall(api.getPaymentData(), callback)
    }
}