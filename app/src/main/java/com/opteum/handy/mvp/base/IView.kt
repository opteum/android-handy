package com.opteum.handy.mvp.base

interface IView {
    fun showProgress()
    fun hideProgress()
    fun showMessage(resMsg: Int)
    fun showMessage(msg: String)
    fun say(resMsg: Int)
    fun say(msg: String)
}