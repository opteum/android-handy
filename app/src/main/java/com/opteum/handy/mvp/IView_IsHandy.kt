package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView

interface IView_IsHandy : IView {
    fun goToTellAbout()
    fun goToFeedback()
}