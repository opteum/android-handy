package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppPrefs
import java.util.concurrent.TimeUnit

class Interactor_IsHandy {
    private val ctx: Context = App.kodein.instance()
    private val pref: AppPrefs = App.kodein.instance()
    private val daysToRequest = ctx.resources.getInteger(R.integer.is_handy_request_days)

    fun isRequestTime(): Boolean {
        val startTimeMs = pref.getLong(R.string.pref_is_handy_request_time_start_ms)
        if (startTimeMs == 0L) {
            pref.putLong(R.string.pref_is_handy_request_time_start_ms, System.currentTimeMillis())
            return false
        }

        if (startTimeMs == -1L) return false

        val msDiff = System.currentTimeMillis() - startTimeMs
        val daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff)

        if (daysDiff >= daysToRequest) {
            pref.putLong(R.string.pref_is_handy_request_time_start_ms, -1L)
            return true
        } else {
            return false
        }
    }
}