package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView

interface IView_Share : IView {
    fun close()
}