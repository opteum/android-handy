package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.metrics.YaHelper
import com.opteum.handy.mvp.base.Presenter

class Presenter_PaymentReject : Presenter<IView_PaymentReject>() {
    private val ctx: Context = App.kodein.instance()
    private val interactor = Interactor_Payment()

    override fun onFirstStart() {}

    fun onClickReasonAnother() {
        view { showAnotherReasonDialog() }
    }

    fun onClickReasonNotInterested() {
        val text = ctx.getString(R.string.msg_cancel_reason1)
        YaHelper.reportPaymentRejectReason(text)
        interactor.sendRejectReason(text, {})
        view { close() }
    }

    fun onClickReasonTooExpensive() {
        val text = ctx.getString(R.string.msg_cancel_reason2)
        YaHelper.reportPaymentRejectReason(text)
        interactor.sendRejectReason(text, {})
        view { close() }
    }

    fun onClickReasonUnconvincingly() {
        val text = ctx.getString(R.string.msg_cancel_reason3)
        YaHelper.reportPaymentRejectReason(text)
        interactor.sendRejectReason(text, {})
        view { close() }
    }

    fun anotherReasonText(text: String) {
        YaHelper.reportPaymentRejectReason(text, true)
        interactor.sendRejectReason(text, {})
        view { close() }
    }
}