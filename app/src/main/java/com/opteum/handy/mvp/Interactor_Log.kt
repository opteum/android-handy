package com.opteum.handy.mvp

import com.github.salomonbrys.kodein.instance
import com.google.gson.Gson
import com.opteum.handy.App
import com.opteum.handy.BuildConfig
import com.opteum.handy.metrics.WalkthroughLogger
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.ui.base.InteractorResult
import com.opteum.handy.utils.AppUtil

class Interactor_Log {
    private val appUtil: AppUtil = App.kodein.instance()
    private val gson: Gson = App.kodein.instance()
    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()

    fun log(events: List<WalkthroughLogger.EventPack.EventItem>) {
        val eventsPack = WalkthroughLogger.EventPack(appUtil.getDeviceId(), BuildConfig.VERSION_NAME, events)
        apiExe.executeApiCall(api.log(gson.toJson(eventsPack)), {})
    }

    fun reportAppMadeItsWork(taxiAppId: String, callback: (result: InteractorResult<Unit>) -> Unit) {
        apiExe.executeApiCall(api.sendReportOrderEvent(taxiAppId), callback)
    }
}