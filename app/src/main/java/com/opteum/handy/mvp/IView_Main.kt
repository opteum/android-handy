package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView

interface IView_Main : IView {
    fun navToStart()
    fun showFeedbackRequestView()
    fun showIsHandyRequestView()
}