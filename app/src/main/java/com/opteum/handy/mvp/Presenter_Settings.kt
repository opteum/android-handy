package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.Presenter

class Presenter_Settings : Presenter<IView_Settings>() {
    private val interactorAuth = Interactor_Auth()

    override fun onFirstStart() {
        view { invalidateAuthButtonsView(interactorAuth.getEmail()) }
    }

    override fun onStarted() {
        val email = interactorAuth.getEmail()
        view { invalidateAuthButtonsView(email) }
    }

    fun onPaymentClick() {
        view { openPaymentView() }
    }

    fun onAboutClick() {
        view { openAboutView() }
    }

    fun onProfileClick() {
        view { openProfileView() }
    }

    fun onAuthActionClick() {
        if (interactorAuth.isTokenExist()) {
            interactorAuth.logout { onLogoutResult() }
        } else {
            view { showLoginOnRegisterDialog() }
        }
    }

    private fun onLogoutResult() {
        view { invalidateAuthButtonsView(interactorAuth.getEmail()) }
    }
}