package com.opteum.handy.mvp

import com.opteum.handy.mvp.base.IView
import com.opteum.handy.network.responses.Response_Profile


interface IView_Profile : IView {
    fun showData(data: Response_Profile)
}