package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.R
import com.opteum.handy.data.AppStrings
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.services.HandyAccessibilityService
import com.opteum.handy.ui.base.ifFalse
import com.opteum.handy.utils.AccessibilityUtil

class Presenter_Main : Presenter<IView_Main>() {
    private val ctx: Context = App.kodein.instance()
    private val interactorFeedback = Interactor_Feedback()
    private val interactorIsHandy = Interactor_IsHandy()
    private val interactorControls = Interactor_Controls()

    override fun onFirstStart() {}

    fun onUserHere() {
        val isEnabled = AccessibilityUtil.isAccessibilityEnabled(ctx, HandyAccessibilityService::class.java)
        isEnabled.ifFalse { view { navToStart() } }
    }

    fun onAppConfigChanged() {
        if (interactorControls.isEnabled()) return

        if (interactorFeedback.isFeedbackRequestTime()) {
            view { showFeedbackRequestView() }
        } else if (interactorIsHandy.isRequestTime()) {
            view { showIsHandyRequestView() }
        }
    }
}