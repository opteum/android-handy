package com.opteum.handy.mvp

import android.content.Context
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.BuildConfig
import com.opteum.handy.network.Api
import com.opteum.handy.network.AppApiCallExecutor
import com.opteum.handy.network.responses.Response_Init
import com.opteum.handy.ui.base.InteractorResult
import com.opteum.handy.utils.LocaleUtil

class Interactor_Init {
    private val ctx: Context = App.kodein.instance()

    private val api: Api = App.kodein.instance()
    private val apiExe: AppApiCallExecutor = App.kodein.instance()

    fun init(callback: (result: InteractorResult<Response_Init>) -> Unit) {
        apiExe.executeApiCall(api.init(BuildConfig.VERSION_CODE.toString(), LocaleUtil.getShortLocale(ctx)), callback)
    }
}