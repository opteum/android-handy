package com.opteum.handy.mvp

import com.opteum.handy.R
import com.opteum.handy.mvp.base.DefaultMvpInteractorResultHandler
import com.opteum.handy.mvp.base.Presenter
import com.opteum.handy.network.responses.Response_Auth
import com.opteum.handy.ui.base.InteractorResult

class Presenter_Auth : Presenter<IView_Auth>() {
    private val interactor = Interactor_Auth()

    override fun onFirstStart() {}

    fun onLoginClick(email: String, password: String) {
        if (email.isBlank() || password.isBlank()) {
            view { showMessage(R.string.err_field_is_empty) }
            return
        }

        view { showProgress() }
        interactor.auth(email, password, this::onLoginResult)
    }

    fun onRegisterClick(email: String, password: String) {
        if (email.isBlank() || password.isBlank()) {
            view { showMessage(R.string.err_field_is_empty) }
            return
        }

        view { showProgress() }
        interactor.register(email, password, { onRegisterResult(it, email, password) })
    }

    private fun onLoginResult(result: InteractorResult<Response_Auth>) {
        view {
            hideProgress()
        }

        if (result.data?.authToken.isNullOrBlank()) {
            interactor.logout { }
            view { showMessage(R.string.text_response_error) }
            return
        }

        view {
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, {
                showMessage(R.string.text_operation_successful)
                close()
            })
        }
    }

    private fun onRegisterResult(result: InteractorResult<Unit>, login: String, password: String) {
        view {
            hideProgress()
            DefaultMvpInteractorResultHandler.doDefaultHandle(result, this, {
                showMessage(R.string.text_operation_successful)
                loginAfterRegister(login, password)
            })
        }
    }

    private fun loginAfterRegister(login: String, password: String) {
        view { showProgress() }
        interactor.auth(login, password, this::onLoginResult)
    }
}