package com.opteum.handy.utils

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.widget.Toast
import com.opteum.handy.R


object AppsUtil {
    fun getAllInstalledApps(ctx: Context): MutableList<ApplicationInfo> {
        return ctx.packageManager.getInstalledApplications(PackageManager.GET_META_DATA)
    }

    fun isInstalled(packageName: String, apps: MutableList<ApplicationInfo>): Boolean {
        return apps.any { it.packageName == packageName }
    }

    fun startApp(ctx: Context, packageName: String) {
        ctx.packageManager.getLaunchIntentForPackage(packageName)?.let {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.component = ComponentName(packageName, it.component.className)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            ctx.startActivity(intent)
        }
    }

    fun resumeToThisApp(ctx: Context) {
        startApp(ctx, ctx.packageName)
    }

    fun rateThisAppOnGooglePlay(activity: Activity) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.packageName))
        val activities = activity.packageManager.queryIntentActivities(intent, 0)

        Toast.makeText(activity, R.string.msg_rate_app_on_google_play, Toast.LENGTH_LONG).show()

        if (activities.size > 0) {
            activity.startActivity(intent)
        } else {
            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.packageName)))
        }
    }
}