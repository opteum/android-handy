package com.opteum.handy.utils

import android.accessibilityservice.AccessibilityServiceInfo
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.view.accessibility.AccessibilityManager


object AccessibilityUtil {
    private val SERVICES_DIR = "services"

    fun isAccessibilityEnabled(ctx: Context, type: Class<*>): Boolean {
        val id = "${ctx.packageName}/.$SERVICES_DIR.${type.simpleName}"
        val ac = (ctx.getSystemService(Context.ACCESSIBILITY_SERVICE) as? AccessibilityManager) ?: return false
        return ac.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK).any { it.id == id }
    }

    fun startAccessibilitySettings(ctx: Context) {
        ctx.startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
    }
}