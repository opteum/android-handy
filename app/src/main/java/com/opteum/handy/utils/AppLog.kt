package com.opteum.handy.utils

import android.util.Log
import com.opteum.handy.BuildConfig

object AppLog {
    fun log(msg: String) {
        if (BuildConfig.DEBUG) Log.d("qwe", msg)
    }

    fun log(tag: String, msg: String) {
        if (BuildConfig.DEBUG) Log.d(tag, msg)
    }
}