package com.opteum.handy.utils

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings

class AppUtil(private val appCtx: Context) {
    private var cacheDeviceId = ""

    @SuppressLint("HardwareIds")
    fun getDeviceId(): String {
        if (cacheDeviceId.isBlank()) {
            cacheDeviceId = Settings.Secure.getString(appCtx.contentResolver, Settings.Secure.ANDROID_ID)
        }

        return cacheDeviceId
    }
}