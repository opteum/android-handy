package com.opteum.handy.utils

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.annotation.TargetApi
import android.graphics.Path
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.os.Handler

object GestureUtil {
    const val GESTURE_MS = 200L

    @TargetApi(Build.VERSION_CODES.N)
    private fun buildTap(x: Int, y: Int): GestureDescription {
        val point = Point(x, y)
        val builder = GestureDescription.Builder()
        val path = Path()
        path.moveTo(point.x.toFloat(), point.y.toFloat())
        builder.addStroke(GestureDescription.StrokeDescription(path, 0L, 1L))
        return builder.build()
    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun buildGestureLine(x1: Int, y1: Int, x2: Int, y2: Int): GestureDescription {
        val point = Point(x1, y1)
        val builder = GestureDescription.Builder()
        val path = Path()
        path.moveTo(point.x.toFloat(), point.y.toFloat())
        path.lineTo(x2.toFloat(), y2.toFloat())
        builder.addStroke(GestureDescription.StrokeDescription(path, 0L, GESTURE_MS))
        return builder.build()
    }

    fun tapOn(bounds: Rect, dispatchGesture: (gesture: GestureDescription, callback: AccessibilityService.GestureResultCallback?, handler: Handler?) -> Boolean) {
        val deltaXHalf = bounds.left + (bounds.right - bounds.left) / 2
        val deltaYHalf = bounds.top + (bounds.bottom - bounds.top) / 2
        dispatchGesture(buildTap(deltaXHalf, deltaYHalf), null, null)
    }

    fun toLeftOrToDown(bounds: Rect, dispatchGesture: (gesture: GestureDescription, callback: AccessibilityService.GestureResultCallback?, handler: Handler?) -> Boolean) {
        var startX = bounds.left + (bounds.right - bounds.left) / 2
        var startY = bounds.top + (bounds.bottom - bounds.top) / 2
        var endX = bounds.left + (bounds.right - bounds.left) / 2
        var endY = bounds.top + (bounds.bottom - bounds.top) / 2

        if (bounds.width() >= bounds.height()) {
            startX = bounds.right - 1
            endX = bounds.left + 1
        } else {
            startY = bounds.top + 1
            endY = bounds.bottom - 1
        }

        dispatchGesture(buildGestureLine(startX, startY, endX, endY), null, null)
    }
}