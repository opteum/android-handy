package com.opteum.handy.utils

import android.content.Context
import android.os.Build
import java.util.*


object LocaleUtil {
    fun getCurrentLocale(context: Context): Locale {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales.get(0)
        } else {
            context.resources.configuration.locale
        }
    }

    fun getShortLocale(context: Context): String {
        return getCurrentLocale(context).language
    }

    fun getRusLocale(): Locale {
        return Locale("ru", "RU")
    }
}
