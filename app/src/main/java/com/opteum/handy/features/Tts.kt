package com.opteum.handy.features

import android.content.Context
import android.os.Build
import android.speech.tts.TextToSpeech
import com.github.salomonbrys.kodein.instance
import com.opteum.handy.App
import com.opteum.handy.data.AppStrings
import com.opteum.handy.metrics.YaHelper

@Suppress("DEPRECATION")
class Tts(ctx: Context) {
    private val strings: AppStrings = App.kodein.instance()
    private val tts = TextToSpeech(ctx, { onStart(it) })

    private fun onStart(status: Int) {
        if (status == TextToSpeech.ERROR) {
            YaHelper.reportError("Не удалось инициализировать tts", Exception())
        }
    }

    fun sayAdd(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_ADD, null, null)
        } else {
            tts.speak(text, TextToSpeech.QUEUE_ADD, null)
        }
    }

    fun sayFlush(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null)
        }
    }

    fun sayAdd(resId: Int) {
        sayAdd(strings[resId])
    }
}